<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ItemStock;
use App\Models\StockHistory;
use DB;
use App\Models\QueingItemStock;

class UploadItemStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:itemstock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Uploading of  ItemStock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //



        //  set_time_limit(0);
        // $fileName = $request->file('data')->getClientOriginalName();

        // $c_folder = substr($fileName, 0,4);
        // $b_folder = substr($fileName, 4,4);
        // $destinationPath = storage_path().'/uploads/inventory/'.$c_folder.'/'.$b_folder;
        
        // if (!\File::exists($destinationPath))
        // {
        //     mkdir($destinationPath, 0755, true); 
        // }

        // $request->file('data')->move($destinationPath, $fileName);

        // $filePath = $destinationPath ."/". $fileName;

        // dd($filePath);
        echo "test" .PHP_EOL;
        $check = QueingItemStock::where('processing',1)->first();
        if(empty($check)) {
            echo "withdata" .PHP_EOL;
           
            $que   = QueingItemStock::where('que',1)->orderBy('created_at','desc')->first();
            $que->processing = 1;
                $que->update();
            $filePath = $que->filename;
            //DB::beginTransaction();
            //try {
               
       

                $reader = ReaderFactory::create(Type::CSV); // for XLSX files
                $reader->setFieldDelimiter('|');
                $reader->open($filePath);
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $row) {
                        // dd($row);
                             $time = microtime();
                 $time = explode(' ', $time);
                $time = $time[1] + $time[0];
                $start = $time;


                        $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                        $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                        $cost = " ";
                        $srp  = " ";
                        if(!empty($row[13])) {

                            $cost = $row[13];
                        }

                        if(!empty($row[14])) {

                            $srp = $row[14];

                        }
                        $in = " ";
                        $out = " ";
                        

                                
                        $data['company_code'] = $company_code;
                        $data['company_name'] = $row[1];
                        $data['branch_code'] = $branch_code;
                        $data['branch_name'] = $row[3];
                        $data['barcode'] = $row[4];
                        $data['begin'] = $row[5];
                        $data['end'] = $row[6];
                        $data['in_qty'] = $in;
                        $data['out_qty'] = $out;
                        $data['date'] = date('Y-m-d', strtotime($row[7]));
                        $data['itemcode'] = $row[8];
                        $data['description'] = $row[9];
                        $data['qty'] = $row[6];
                        $data['department'] = $row[7];
                        $data['category'] = $row[8];
                        $data['brand'] = $row[9];
                        $data['department'] = $row[10];
                        $data['category'] = $row[11];
                        $data['brand'] = $row[12];  
                        $data['cost'] = $cost;
                        $data['srp'] = $srp;
                        $data['movement_description'] = " ";
                        $data['purpose']  = " ";
                        $data['move_ref'] = " ";
                        // dd($data);
                         // echo "Item Stock Getting item" .PHP_EOL;
                        $item = ItemStock::getItem($data);

                        
                        if(!empty($item)){
                            $data['item_stock_id'] = $item->id;
                            StockHistory::updateHistory($data);
                        }else{
                            ItemStock::addStocks($data);
                            $item = ItemStock::getItem($data);
                            $data['item_stock_id'] = $item->id;
                            StockHistory::updateHistory($data);

                        }
                        $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $finish = $time;
            $total_time = round(($finish - $start), 4);
            echo $total_time .PHP_EOL;
                        
                    }

                }
                $reader->close();
               // DB::commit();
                $que->que = 0;
                $que->processing = 0;
                $que->update();
                //return response()->json(array('msg' => 'file uploaded', 'status' => 0));
            // } catch (Exception $e) {
            //     DB::rollback();
            //     return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
            // }
        }

    }
}
