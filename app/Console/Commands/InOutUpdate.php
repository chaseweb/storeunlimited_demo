<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use App\Models\StockHistory;
class InOutUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:inout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that update In and Out Qty';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

//           Artisan::call('db:seed', [
//     '--class' => 'InOutSeeder'
// ]);

              set_time_limit(0);
        ini_set('memory_limit', '-1');
        $take = 1000; // adjust this however you choose
        $skip = 0;

           while($rows = StockHistory::whereRaw('begining != ending')->where('checked',0)->skip($skip*$take)
            ->take($take)->get())
                {
                    if(count($rows) == 0){
                        break;
                    }
                    $skip ++;
                   
                    foreach($rows as $history)
                    {
                    


                            $in_qty = 0 ; 
                            $out_qty = 0;
                            echo $history->id .PHP_EOL;
                            $stock_history = StockHistory::where('id' , $history->id)->first();


                               

                                    if($stock_history->begining > $stock_history->ending) {
                                    
                                        $out_qty = $stock_history->begining - $stock_history->ending;
                                        
                                        
                                    }
                                    else if ($stock_history->begining < $stock_history->ending) {

                                        $in_qty = $stock_history->ending - $stock_history->begining;
                                    }
                                    


                               
                                        
                            $stock_history->in_qty = $in_qty;
                            $stock_history->out_qty = $out_qty;
                            $stock_history->checked = 1;
                            $stock_history->update();



                    }

                 
                    
                }
    }
}
