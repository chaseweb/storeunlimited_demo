<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ItemStock;
use App\Models\StockHistory;

class GenerateItemStockHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:itemhistory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Generating Item Beginning and Ending Inventory');

        $stocks = StockHistory::getNoEnding();

        foreach ($stocks as $stock) {
            $stock->ending = $stock->begining;
            $stock->update();
        }

        $items = ItemStock::all();
        foreach ($items as $item) {
            $date = date('Y-m-d');
            $data['item_stock_id'] = $item->id;
            $data['transaction_date'] = $date;
            if(!StockHistory::alreadyExist($data)){
                StockHistory::create(['item_stock_id' => $item->id,
                'begining' => $item->qty,
                'ending' => null,
                'transaction_date' => $date]);
            }
            
        }
    }
}
