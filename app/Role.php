<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public static function search(){
        return self::all();
    }

    public static function getList(){
    	return self::lists('display_name', 'id');
    }
}
