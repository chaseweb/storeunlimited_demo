<?php

namespace App\Listeners;

use App\Events\GenerateExcel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

class ExcelGenerated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $items;
    public function __construct($items)
    {
        $this->items = $items;
    }

    /**
     * Handle the event.
     *
     * @param  GenerateExcel  $event
     * @return void
     */
    public function handle(GenerateExcel $event)
    {
        $writer = WriterFactory::create(Type::XLSX);
            $writer->openToBrowser("SALES_SUMMARY_REPORT_SALES".'.xlsx');
            $style_header = (new StyleBuilder())           
               ->setFontSize(14)
               ->setFontName('Calibri')
               ->setFontColor(Color::BLACK)           
               ->build();   
            $style_details = (new StyleBuilder())           
               ->setFontSize(11)
               ->setFontName('Agency FB')
               ->setFontColor(Color::BLACK)           
               ->build();
            $writer->addRowWithStyle(array('Item Code','Qty'),$style_header);
                foreach($items as $item){   
                    $data[1] =  $item->itemcode;
                    $data[2] =  $item->qty;
                    $writer->addRowWithStyle($data,$style_details);
                }
            $sheet = $writer->getCurrentSheet();
            $sheet->setName('INVENTORY_SUMMARY_REPORT_');
            $writer->close();
    }
}
