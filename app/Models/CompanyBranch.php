<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CompanyBranch extends Model
{
    protected $fillable = [
        'company_id', 
        'branch_code',
        'branch',
        'area'
    ];

    public function company(){
    	return $this->belongsTo('App\Models\Company');
    }
    
    public static function search(){
        return self::all();
    }

    public static function getBranch($branch_code){
        return self::where('branch_code', $branch_code)->first();
    }


     public static function getCompaniesBranch($request){
 
        return self::select('branch', 'branch_code')
            ->whereIn('company_id', $request->companies)
            ->orderBy('branch')
            ->groupBy('branch_code')
            ->lists('branch', 'branch_code');
    }

    public static function getAll()
    {
        return self::all();
    }
}
