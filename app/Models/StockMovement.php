<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMovement extends Model
{
    protected $fillable = ['item_stock_id', 'previous_stock', 'move', 'resulting_stock', 'movement_description', 'purpose', 'move_ref'];

    public static function getHistory($id){
    	return self::where('item_stock_id', $id)
    		->orderBy('created_at', 'desc')
    		->get();
    }
}
