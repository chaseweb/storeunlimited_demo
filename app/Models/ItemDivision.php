<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemDivision extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_division_code',
        'item_division'
    ];
}
