<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSize extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_size_code',
        'item_size'
    ];
}
