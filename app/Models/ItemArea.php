<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemArea extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_area_code',
        'item_area'
    ];
}
