<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailContent extends Model
{
    //

    protected $fillable = [
        'user_id', 
        'branch',
        'branch_code',
        'date',
        'reason',
        
    ];


    public function user()
    {
       return $this->belongsTo('App\User');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\CompanyBranch', 'branch_code', 'branch_code');
    }
}
