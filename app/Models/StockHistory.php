<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    protected $fillable = ['item_stock_id', 'begining', 'ending', 'transaction_date' ,'in_qty','out_qty'];

    public static function alreadyExist($data){
    	$record = self::where('item_stock_id', $data['item_stock_id'])
    		->where('transaction_date', $data['transaction_date'])
    		->get();

    	if(count($record)>0){
    		return true;
    	}else{
    		return false;
    	}
    }

    public static function getNoEnding(){
    	return self::whereNull('ending')
    		->get();
    }

    public static function getHistory($id){
    	return self::where('item_stock_id', $id)
    		->orderBy('transaction_date', 'desc')
    		->get();
    }

    public static function gettime(){
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        return   $time;
    }

    public static function updateHistory($data){

            
            // $start = self::gettime();

            $history = self::select('begining','ending','transaction_date','checked')
            ->where('item_stock_id', $data['item_stock_id'])
            ->where('transaction_date', $data['date'])
            ->first();

            // $finish = self::gettime();
            // $total_time = round(($finish - $start), 4);
            // echo "Select History ".$total_time .PHP_EOL;


             // echo "Select History" .PHP_EOL;

        if(empty($history)){
            // echo "If Empty History" .PHP_EOL;
            self::create(['item_stock_id' => $data['item_stock_id'],
                'begining' => $data['begin'],
                'ending' => $data['end'],
                'transaction_date' => $data['date']]);
        }else{

            // echo "Update History" .PHP_EOL;
            $history->begining = $data['begin'];
            $history->ending = $data['end'];
            $history->checked = 0;
            $history->save();
        }

        // $start = self::gettime();

        $latest = self::select('ending','item_stock_id')
            ->where('item_stock_id', $data['item_stock_id'])
            ->orderBy('transaction_date', 'desc')
            ->first();

             $finish = self::gettime();
            // $total_time = round(($finish - $start), 4);
            // echo "Select Sorting ".$total_time .PHP_EOL;

                  // $start = self::gettime();

        $item = ItemStock::find($latest->item_stock_id);
        // dd($item);
        $item->qty = $data['end'];
        $item->save();

   // $finish = self::gettime();
   //          $total_time = round(($finish - $start), 4);
   //          echo "update stock ".$total_time .PHP_EOL;

           // $time = explode(' ', $time);
           //  $time = $time[1] + $time[0];
           //  $finish = $time;
           //  $total_time = round(($finish - $start), 4);
           //  echo "Select  Item Stock".$total_time .PHP_EOL;

    }
}
