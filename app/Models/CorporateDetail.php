<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateDetail extends Model
{
    //

    protected $fillable = [
        'sale_summary_id', 
        'account_no',
        'amount',
        'created_at',
        'updated_at'
    ];
}
