<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class SalesInventoryExcel extends Model
{
    protected $fillable = [
        'type', 
        'branch_code',
        'file',
        'transact_date'
    ];

    public static function getFiles($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        $companies = $request->company;

        if(($request->has('branches'))&& (!empty($request->branches))){
            $_branches = $request->branches;
        }else{
            $_branches = $branches;
        }
        $com  ='';
        if(!empty($companies)){
            $com = " and companies.company_code in ('".implode("','", $companies)."')";
        }

        $br = [];
        foreach ($_branches as $branch) {
        	$br[] = substr($branch,4,4);
        }

        $se_branches = implode("','", $br);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $query = sprintf("select * from sales_inventory_excels
        	where DATE(transact_date) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')",
        $date_from,$date_to,$se_branches);
        
        return DB::select(DB::raw($query));
        
    }
}
