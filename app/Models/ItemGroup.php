<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemGroup extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_group_code',
        'item_group'
    ];
}
