<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManualInventory extends Model
{
    protected $fillable = [
        'branch_id', 
        'voucher_number',
        'filename'
    ];

    public static function getFiles(){
    	return self::select('manual_inventories.id', 'manual_inventories.filename','manual_inventories.voucher_number','manual_inventories.created_at',
    		'company_branches.branch_code', 'company_branches.branch')
    		->join('company_branches', 'company_branches.id', '=', 'manual_inventories.branch_id')
    		->get();
    }
}
