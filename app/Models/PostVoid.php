<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostVoid extends Model
{
    protected $fillable = [
        'company_code', 'company_name', 'branch_code', 'branch_name', 'terminal_code', 'terminal_no', 'user', 'transaction_no', 
        'local_time'
    ];

    public static function recordExist($data){
    	return self::where('company_code', $data['company_code'])
    		->where('branch_code', $data['branch_code'])
    		->where('terminal_code', $data['terminal_code'])
    		->where('user', $data['user'])
    		->where('transaction_no', $data['transaction_no'])
    		->first();
    }
    
    public static function search(){
    	return self::all();
    }

    public static function ZapPerDay($company_code, $branch_code, $date_from, $date_to)
    {
        return self::where('company_code',$company_code)->where('branch_code',$branch_code)
            ->whereBetween('local_time',[$date_from, $date_to])->get();
    }
}
