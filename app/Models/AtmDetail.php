<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AtmDetail extends Model
{
    //
    
      protected $fillable = [
        'sale_summary_id', 
        'card_number',
        'card_name',
        'bank_name',
        'bank',
        'amount',
        'created_at',
        'updated_at'
    ];

}
