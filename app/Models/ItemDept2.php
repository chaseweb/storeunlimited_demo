<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemDept2 extends Model
{
	public $table = 'item_dept2';
	
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_dept2_code',
        'item_dept2'
    ];


}
