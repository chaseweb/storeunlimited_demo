<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCategory2 extends Model
{
	public $table = 'item_category2';
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_category2_code',
        'item_category2'
    ];
}
