<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GrahamCampbell\Flysystem\Facades\Flysystem;
use App\Models\SalesInventory;

class SalesInventoryController extends Controller
{
    public function store(Request $request){
      
    	$fileName = $request->file('data')->getClientOriginalName();

        $destinationPath = storage_path().'/uploads/salesinventory/';
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       	
       	try {
       		// $stream = fopen($filePath, 'r+');
			// Flysystem::connection('sftp')->put($fileName, $stream);
            $type = 1;
            if(substr($fileName,0,9) == "INVENTORY" || substr($fileName,0,9) == "inventory") ){
                $type = 2;
                $branch_code = substr($fileName,12,4);
                $transact_date = substr($fileName,17,4)."-".substr($fileName,21,2)."-".substr($fileName,23,2);
            }else{
                $branch_code = substr($fileName,8,4);
                $transact_date = substr($fileName,13,4)."-".substr($fileName,17,2)."-".substr($fileName,19,2);
            }

            SalesInventory::firstOrCreate(['type' => $type, 
                'branch_code' => $branch_code, 
                'file' => $fileName,
                'transact_date' => $transact_date]);

			return response()->json(array('msg' => 'file uploaded', 'status' => 0));
       	} catch (Exception $e) {
       		return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
       	}
		

		
    }
}
