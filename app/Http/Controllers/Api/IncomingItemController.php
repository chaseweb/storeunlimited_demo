<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

use App\Models\IncomingItem;
use App\Models\OutgoingItem;
use App\Models\ItemStock;
use App\Models\PurchaseOrder;
use DB;

class IncomingItemController extends Controller
{
    public function stocksstorebranch(Request $request){
        $data['selection'] = IncomingItem::getCompaniesBranch($request);
        return \Response::json($data,200);
    }

    public function stocksstorecategory(Request $request){
        $data['selection'] = IncomingItem::getCompaniesBranchCategory($request);
        return \Response::json($data,200);
    }

    public function stocksstorebrand(Request $request){
        $data['selection'] = IncomingItem::getCompaniesBranchCategoryBrand($request);
        return \Response::json($data,200);
    }
    
    public function uploadincoming(Request $request){
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/incoming/'.$c_folder.'/'.$b_folder.'/'.$t_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $terminal_code = $branch_code.str_pad($row[4], 2, "0", STR_PAD_LEFT);

                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['terminal_code'] = $terminal_code;
                    $data['terminal_no'] = $row[4];
                    $data['user'] = $row[5];
                    $data['incoming_no'] = $row[6];
                    $data['po_no'] = $row[7];
                    $data['outgoing_no'] = $row[8];
                    // $data['from_company_code'] = str_pad($row[9], 4, "0", STR_PAD_LEFT);
                    // $data['from_company'] = $row[10];
                    // $data['from_branch_code'] = str_pad($row[9], 4, "0", STR_PAD_LEFT).str_pad($row[11], 4, "0", STR_PAD_LEFT);
                    // $data['from_branch'] = $row[12];
                    $data['local_time'] = date('Y-m-d H:i:s', strtotime($row[9].' '.$row[10]));
                    $data['barcode'] = $row[11];
                    $data['itemcode'] = $row[12];
                    $data['description'] = $row[13];
                    $data['qty'] = $row[14];
                    $data['price'] = $row[15];
                    $data['amount'] = $row[16];
                    $data['box_no'] = $row[17];
                    $data['pouch_no'] = $row[18];
                    $data['unique_code'] = $row[19];
                    $data['purpose'] = $row[20];
                    $data['local_date'] = date('Y-m-d', strtotime($row[9]));
                    $data['department'] = $row[21];
                    $data['category'] = $row[22];
                    $data['brand'] = $row[23];



                    $item = IncomingItem::recordExist($data);
                    if(empty($item)){
                        IncomingItem::create([
                        'company_code' => $data['company_code'],
                        'company_name' => $data['company_name'],
                        'branch_code' => $data['branch_code'],
                        'branch_name' => $data['branch_name'],
                        'terminal_code' => $data['terminal_code'],
                        'terminal_no' => $data['terminal_no'],
                        'user' => $data['user'],
                        'incoming_no' => $data['incoming_no'],
                        'po_no' => $data['po_no'],
                        'outgoing_no' => $data['outgoing_no'],
                        // 'from_company_code' => $data['from_company_code'],
                        // 'from_company' => $data['from_company'],
                        // 'from_branch_code' => $data['from_branch_code'],
                        // 'from_branch' => $data['from_branch'],
                        'local_time' => $data['local_time'],
                        'barcode' => $data['barcode'],
                        'itemcode' => $data['itemcode'],
                        'description' => $data['description'],
                        'qty' => $data['qty'],
                        'price' => $data['price'],
                        'amount' => $data['amount'],
                        'box_no' => $data['box_no'],
                        'pouch_no' => $data['pouch_no'],
                        'unique_code' => $data['unique_code'],
                        'local_date' => $data['local_date'],
                        'purpose' => $data['purpose'],
                        'department' => $data['department'],
                        'category' => $data['category'],
                        'brand' => $data['brand']
                        
                        ]);

                        $data['movement_description'] = 'Incoming Items';
                        $data['move_ref'] = $data['incoming_no'];

                        ItemStock::addStocks($data);

                        // add incoming no on po item
                        $item_po = PurchaseOrder::getItemPo($data);
                        if(!empty($item_po)){
                            $item_po->incoming_no = $data['incoming_no'];
                            $item_po->save();
                        }

                        // add outgoing no on incoming item
                        $item_outgo = OutgoingItem::getItemOutGo($data);
                        if(!empty($item_outgo)){
                            $item_outgo->incoming_no = $data['incoming_no'];
                            $item_outgo->save();
                        }
                    }

                    
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }
    }
}
