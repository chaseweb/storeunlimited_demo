<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleDetail;

class UpsellController extends Controller
{
    public function index(){
        ini_set('memory_limit', '-1');
    	$date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
    	$upsells = SaleDetail::getUpsell();    	
    	$branches = SaleDetail::getUpsellBranches();
        $sel_types = [];
        //asas
    	$total_qty = 0; 

        foreach ($upsells as $upsell) {
            $total_qty = $total_qty + $upsell->qty;
        }     

    	return view('upsell.index',compact('upsells','total_qty','date_from','date_to','branches','sel_types'));
    }

    public function store(Request $request){
        ini_set('memory_limit', '-1');
        
    	$request->flash();
        $date_from = $request->date_from;
        $date_to = $request->date_to;
    	$sel_types[] = $request->type;

    	$branches = SaleDetail::getUpsellBranches();

    	$upsells = SaleDetail::searchUpsell($request,$date_from,$date_to);

        $total_qty = 0; 

        foreach ($upsells as $upsell) {
            $total_qty = $total_qty + $upsell->qty;
        }       

    	return view('upsell.index',compact('upsells','total_qty','date_from','date_to','branches','sel_types'));
    }
}
