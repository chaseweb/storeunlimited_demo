<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleSummary;
use App\Models\UserBranch;
use App\Models\SaleDetail;
use App\Models\CompanyBranch;
use Auth;

class SalesSummaryController extends Controller
{
    public function index(Request $request){
        
    	$date_from = date('m/d/Y');
    	$date_to = date('m/d/Y');
    	$companies = SaleSummary::getCompanies();
        $item = " ";
        $summaries = SaleSummary::salessummary($date_from, $date_to, $request);        
    	return view('sales.index', compact( 'companies', 'date_from', 'date_to', 'summaries','item'));
    }

    public function store(Request $request){
    	$request->flash();

    	$date_from = $request->date_from;
    	$date_to = $request->date_to;
    	$sel_branches = $request->branch;
    	$companies = SaleSummary::getCompanies();

        $summaries = SaleSummary::salessummary($date_from, $date_to, $request);
        $submit_type = $request->get('submit');
        $item = $request->keyword;


    	if ($submit_type == 2){
    		$data = [];

            foreach ($summaries as $key=>$summary) {

               $data[$key] = ['Company'           => $summary->company,
                     'Branch'            => $summary->branch,
                     'Total Gross Sales' => number_format($summary->net_amount + $summary->return_exchange_amount,2),
                     'Total Sales Refund' => number_format($summary->refund_amount,2),
                     'Total Return Exchange Sales' => number_format($summary->return_exchange_amount,2),
                     'Total Net Sales' => number_format($summary->net_amount - $summary->refund_amount,2)

               ];
               
            }

         
    		\Excel::create('Branch Sales Summary Report', function($excel)  use ($data){
            $excel->sheet('Sheet1', function($sheet) use($data) {
                $sheet->fromArray($data, null, 'A1', false);
            });
        })->download('xls');
    	}else{
    		return view('sales.index', compact( 'companies', 'sel_branches', 'date_from', 'date_to', 'summaries','item'));
    	}
    }

    public function show(Request $request, $id){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        if(in_array($id, $branches)){
            $branch = CompanyBranch::getBranch($id);
            $date_from = date("Y-m-d", $request->from);
            $date_to = date("Y-m-d", $request->to);
            $items = SaleSummary::getBranchSales($date_from, $date_to, $id,$request->item);
            
            return view('sales.show', compact( 'items', 'date_from', 'date_to', 'branch'));
        }else{
            abort(404);
        }
    }

    public function items(Request $request, $id){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        if(in_array($id, $branches)){
            $branch = CompanyBranch::getBranch($id);
            $date_from = date("Y-m-d", $request->from);
            $date_to = date("Y-m-d", $request->to);
            $items = SaleDetail::getDetails($date_from, $date_to, $id,$request->item);
            return view('sales.items', compact( 'items', 'date_from', 'date_to', 'branch','id'));
        }else{
            abort(404);
        }
    }

    public function transaction(Request $request, $id){
         ini_set('memory_limit','-1');
        $summary = SaleSummary::find($id);
        $date_from = date("Y-m-d", $request->from);
        $date_to = date("Y-m-d", $request->to);
        $sale = SaleSummary::with('details')->findOrFail($id);
        return view('sales.details',compact('sale', 'date_from', 'date_to', 'summary'));
    }

    public function download_items_sales(Request $request){

        $date_to = $request->date_to;
        $date_from = $request->date_from;
        $branch = CompanyBranch::where('branch_code',$request->branch_code)->first();
        $filename = "SALES_".$branch->branch."_FROM".$request->date_from."_TO".$request->date_to;
        $items = SaleDetail::consolidatedItemReport($request->date_from, $request->date_to, $request->branch_code);


                 \Excel::create($filename, function($excel)  use ($items,$branch,$date_from,$date_to){
                        $excel->sheet($branch->branch, function($sheet) use($items,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("SALES"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("BARCODE","ITEM CODE","DESCRIPTION","QTY","AMOUNT"));

                            $cnt = 7;
                            $total = 0;
                            foreach ($items as $item) {
                                    $sheet->row($cnt,array(
                                        $item->barcode,
                                        $item->itemcode,
                                        $item->description,
                                        $item->total_qty,
                                        $item->total_net_amount,
                                       

                                    ));
                                    $total = $total + $item->total_net_amount;

                                    $cnt++;
                            }

                            $sheet->mergeCells('A1:E1');
                            $sheet->cell($cnt, function($cell) {
                                $cell->setAlignment('center');
                                $cell->setBackground('#3498db');
                        
                             });
                            $sheet->row($cnt,array(                        
                            ""," ","TOTAL "," ",$total
                            )); 

                        });
                    })->export('xlsx');
        
    }
}
