<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ReturnExchangeItem;

class ReturnExchnageReportController extends Controller
{
    public function index(){
    	$items = ReturnExchangeItem::search();
    	return view('retex.index', compact('items'));
    }
}
