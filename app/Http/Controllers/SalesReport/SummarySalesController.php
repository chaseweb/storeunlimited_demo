<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\UserBranch;
use App\Models\CompanyBranch;
use App\Models\SaleSummary;

use Auth;
class SummarySalesController extends Controller
{
    //as

public static function index(Request $request)
      {

        $date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        $branches  = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.sales.summary_sales',compact('date_from','branches','date_to'));

    }


    public static function post(Request  $request) 
    {
    	set_time_limit(0);
        ini_set('memory_limit', '1G');
        $date_from      = $request->date_from;
        $date_to        = $request->date_to;
        $branch        =  CompanyBranch::where('branch_code',$request->branch)->first();

        $filename = "Daily Summary of Sales Transactions";
     
        $sales = SaleSummary::summaryDailySales($date_from,$date_to,$branch->branch_code);

        \Excel::create($filename, function($excel)  use ($branch,$date_from,$date_to,$sales){
            $excel->sheet("DETAILED DAILY SALES REPORT", function($sheet) use($date_from,$branch,$date_to,$sales) {
                            $sheet->setStyle(array(
                                'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  8
                                  
                                )
                            ));
                            $sheet->setWidth(array(
                                'A'     =>  30,
                                'B'     =>  20,
                                'C'     =>  20,
                                'D'     =>  20,
                                'E'     =>  5,
                                'F'     =>  20,
                                'G'     =>  20,
                                'H'     =>  20,
                                'I'     =>  20,
                                'J'     =>  5,
                                'K'     => 20,
                                'L'     => 20,
                                'M'     => 20,
                                'N'     => 5,
                                'O'     => 20,
                                'P'     => 20,
                                'Q'     => 20,
                                'R'     =>  5,
                                'S'     => 20,
                                'T'     => 20,
                                'U'     => 20,
                            ));
                           	$sheet->mergeCells('B5:C5');
                            $sheet->mergeCells('F5:I5');
                            $sheet->mergeCells('K5:M5');
                            $sheet->mergeCells('O5:Q5');
                            $sheet->mergeCells('S5:U5');
                            $sheet->setBorder('A5:D5', 'thin');
                            $sheet->setBorder('F5:I5', 'thin');
                       
                            $sheet->setBorder('K5:M5', 'thin');
                            $sheet->setBorder('O5:Q5', 'thin');
                            $sheet->setBorder('S5:U5', 'thin');


                            $sheet->setBorder('A6:D6', 'thin');
                            $sheet->setBorder('F6:I6', 'thin');
                    
                            $sheet->setBorder('K6:M6', 'thin');
                            $sheet->setBorder('O6:Q6', 'thin');
                            $sheet->setBorder('S6:U6', 'thin');

                            $sheet->cells('A5:U5', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });
                            $sheet->cells('A6:U6', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });

                            $sheet->row(1, array("PANDORA - ".$branch->branch));
                            $sheet->row(2, array("SUMMARY OF DAILY SALES REPORT "));
                            $sheet->row(3, array("FOR THE PERIOD ".$date_from." to ".$date_to));
                        
                            $sheet->row(5,array('' ,'SALES INVOICE NOS.','','CANCELLED','','VAT EXEMPT TRANSACTIONS',' ',' ',' ',' ','CASH SALES',' ', ' ',' ','CHARGE SALES',' ',' ',' ','GRAND TOTAL'));

                            $sheet->row(6,array('DATE','FROM','TO','INVOICE',' ','SENIOR CITIZEN','PWD','DIPLOMAT','TOTAL',' ', 'VAT IN','VAT EXEMPT','TOTAL',' ','VAT IN','VAT EXEMPT','TOTAL','','VAT IN' ,'VAT EXEMPT' ,'TOTAL'));


                            $cnt = 8;
                            $last = 0;

                            if(count($sales)){

                            	foreach ($sales as $data) {

                            		$senior = $data->senior_amount ;
                                	$pwd = $data->pwd_amount;
                                	$diplomat = $data->diplomat_amount ;
                                	$total_exempt = $senior + $pwd + $diplomat;

                                	$cash_vatin = $data->cash_vat_in;
                                	$cash_vatex  = $data->cash_vat_exempt;
                                	$cash_total = $cash_vatin + $cash_vatex;

                                	$charge_vatin = $data->charge_vat_in;
                                	$charge_vatex  = $data->charge_vat_exempt;
                                	$charge_total = $charge_vatin + $charge_vatex;



	                                $grand_vatin  = $cash_vatin +  $charge_vatin;
	                                $grand_vatex  =  $charge_vatex + $cash_vatex;
	                                $grand_total  = $grand_vatin + $grand_vatex;


                            		# code...



                            		 $sheet->row($cnt,array(
                                    
                                        date('d-M',strtotime($data->local_time)),
                                        $data->from_invoice,
                                        $data->to_invoice,
                                        ' ',
                                        ' ',
                                        $senior,
                                        $pwd,
                                        $diplomat,
                                        $total_exempt,
                                        ' ',
                                        $cash_vatin,
                                        $cash_vatex,
                                        $cash_total,
                                        ' ',
                                        $charge_vatin,
                                        $charge_vatex,
                                        $charge_total,
                                        ' ',
                                        $grand_vatin,
                                        $grand_vatex,
                                        $grand_total,

                                        
                                    

                                    ));
                               
                                 	$last =    $cnt++;
                            	}

                            	 $cnt++;

		                         if($last > 0) {
		                            $q = sprintf("=SUM(U%s:U%s)",8,$last);
		                            $b = sprintf("A%s:U%s",$cnt,$cnt);
		                            $sheet->setBorder($b, 'thin');
		                            $sheet->row($cnt,array('TOTAL','','',' ',' ',' ','','','',' ', ' ',' ','',' ',' ',' ','','',' ' ,' ' ,$q));
		                        }



                            }
                             
                            
            });
                            
        })->export('xls');
        
        



    }


}
