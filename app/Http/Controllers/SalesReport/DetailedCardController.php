<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;


use App\Models\UserBranch;
use App\Models\CompanyBranch;
use App\Models\SaleSummary;

class DetailedCardController extends Controller
{
    //

 	public static function index(Request $request)
      {

        $date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        $branches  = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.sales.detailed_card',compact('date_from','branches','date_to'));

    }


    public static function post(Request $request) {

    	set_time_limit(0);
        ini_set('memory_limit', '1G');
        $date_from      = $request->date_from;
        $date_to        = $request->date_to;
        $branch         =  CompanyBranch::where('branch_code',$request->branch)->first();
        $sales          =  SaleSummary::detailedCardSales($date_from,$date_to,$branch->branch_code); 
        $filename = "DETAILED DAILY CREDIT CARD TX - ".$branch->branch;
        $first_date = " ";
        $last_date = date('d-M',strtotime(SaleSummary::getMinDate($date_from,$date_to,$branch->branch_code)));


      // dd($sales); die;

          \Excel::create($filename, function($excel)  use ($sales,$branch,$date_from,$date_to,$first_date,$last_date){
            $excel->sheet("DETAILED DAILY CREDIT CARD TX", function($sheet) use($sales,$date_from,$branch,$date_to,$first_date,$last_date) {
                          	 $sheet->setStyle(array(
                                'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  8
                                  
                                )
                            ));

                          	 $sheet->setWidth(array(
                                'A'     =>  30,
                                'B'     =>  20,
                                'C'     =>  20,
                                'D'     =>  20,
                                'E'     =>  20,
                                'F'     =>  20,
                                'G'     =>  5,
                                'H'     =>  20,
                                'I'     =>  20,
                                'J'     =>  20,
                                'K'     => 5,
                                'L'     => 20,
                                'M'     => 20,
                                'N'     => 20,
                                'O'     => 20,
                                'P'     => 5,
                                'Q'     => 20,
                                'R'     => 20,
                                'S'     => 20,
                                'T'     => 20,
                                'U'     => 5,
                                'V'     => 20,
                                'W'     => 20,
                                'X'     => 20,
                                'Y'     => 20,
                                'Z'     => 5,
                                'AA'     => 20,
                                'AB'     => 20,
                                'AC'     => 20,
                                'AD'     => 20,
                                'AE'     => 5,
                                'AF'     => 20,
                                'AG'     => 20,
                                'AH'     => 20,
                                'AI'     => 20,
                                'AJ'     => 5,
                                'AK'     => 20,
                                'AL'     => 20,
                                'AM'     => 20,
                                'AN'     => 20,
                                'AO'     => 5,
                                'AP'     => 20,
                                'AQ'     => 20,
                                'AR'     => 20,
                                'AS'     => 20,
                                'AT'     => 5,
                                'AU'     => 20,
                                'AV'     => 20,
                                'AW'     => 20,
                                'AX'     => 20,
                                'AY'     => 5,
                                'AZ'     => 20,
                                'BA'     => 20,
                                'BB'     => 20,
                                'BC'     => 20,
                                'BD'     => 5,
                                'BE'     => 20,
                                'BF'     => 20,
                                'BG'     => 20,
                                'BH'     => 20,
                                'BI'     => 5,
                                'BJ'     => 20,
                                'BK'     => 20,
                                'BL'     => 20,
                                'BM'     => 20,
                                'BN'     => 5,
                                'BO'     => 20,
                                'BP'     => 20,
                                'BQ'     => 20,
                                'BR'     => 20,
                                'BS'     => 5,
                                'BT'     => 20,
                                'BU'     => 20,
                                'BV'     => 20,
                                'BW'     => 20,
                                'BX'     => 5,
                                'BY'     => 20,
                                'BZ'     => 20,
                                'CA'     => 20,
                                'CB'     => 20,
                                'CC'     => 5,
                                'CD'     => 20,
                                'CE'     => 20,
                                'CF'     => 20,
                                'CG'     => 20,
                                'CH'     => 5,
                                'CI'     => 20,
                                'CJ'     => 20,
                                'CK'     => 20,
                                'CL'     => 20,
                                'CM'     => 5,
                                'CN'     => 20,
                                'CO'     => 20,
                                'CP'     => 20,
                                'CQ'     => 20,
                                'CR'     => 5,
                                'CS'     => 20,
                                'CT'     => 20,
                                'CU'     => 20,
                                'CV'     => 20,
                                'CW'     => 5,
                                'CX'     => 20,
                                'CY'     => 20,
                                'CZ'     => 20,
                                'DA'     => 20,
                                'DB'     => 5,
                                'DC'     => 20,
                                'DD'     => 20,
                                'DE'     => 20,
                                'DF'     => 20,
                                'DG'     => 5,
                                'DH'     => 20,
                                'DI'     => 20,
                                'DJ'     => 20,
                                'DK'     => 20,
                                'DL'     => 5,
                                'DM'     => 20,
                                'DN'     => 20,
                                'DO'     => 20,
                                'DP'     => 20,
                                'DQ'     => 5,
                                'DR'     => 20,
                                'DS'     => 20,
                                'DT'     => 20,
                                'DU'     => 20,
                                'DV'     => 5,
                                'DW'     => 20,
                                'DX'     => 20,
                                'DY'     => 20,
                                'DZ'     => 20,
                                'EA'     => 5,
                                'EB'     => 20,
                                'EC'     => 20,
                                'ED'     => 20,
                                'EE'     => 20,
                                'EF'     => 5,
                                'EG'     => 20,
                                'EH'     => 20,
                                'EI'     => 20,
                                'EJ'     => 20,
                                'EK'     => 5,
                                'EL'     => 20,
                                'EM'     => 20,
                                'EN'     => 20,
                                'EO'     => 20,
                                'EP'     => 5,
                                'EQ'     => 20,
                                'ER'     => 20,
                                'ES'     => 20,
                                'ET'     => 20,
                                'EU'     => 5,
                                'EV'     => 20,
                                'EW'     => 20,
                                'EX'     => 20,
                                'EY'     => 20,
                                'EZ'     => 5,
                                'FA'     => 20,
                                'FB'     => 20,
                                'FC'     => 20,
                                'FD'     => 20,
                                'FE'     => 5,
                                'FF'     => 20,
                                'FG'     => 20,
                                'FH'     => 20,
                                'FI'     => 20,
                                'FJ'     => 5,
                                'FK'     => 20,
                                'FL'     => 20,
                                'FM'     => 20,
                                'FN'     => 20,
                                'FO'     => 5,
                                'FP'     => 20,
                                'FQ'     => 20,
                                'FR'     => 20,
                                'FS'     => 20,
                                





                            ));



                          	$sheet->mergeCells('H5:J5');
                            $sheet->mergeCells('L5:O5');
                            $sheet->mergeCells('Q5:T5');
                            $sheet->mergeCells('V5:Y5');
                            $sheet->mergeCells('AA5:AD5');
                            $sheet->mergeCells('AF5:AI5');
                            $sheet->mergeCells('AK5:AN5');
                            $sheet->mergeCells('AP5:AS5');
                            $sheet->mergeCells('AU5:AX5');
                            $sheet->mergeCells('AZ5:BC5');
                            $sheet->mergeCells('BE5:BH5');
                            $sheet->mergeCells('BJ5:BM5');
                            $sheet->mergeCells('BO5:BR5');
                            $sheet->mergeCells('BT5:BW5');
                            $sheet->mergeCells('BY5:CB5');
                            $sheet->mergeCells('CD5:CG5');
                            $sheet->mergeCells('CI5:CL5');
                            $sheet->mergeCells('CN5:CQ5');
                            $sheet->mergeCells('CS5:CV5');
                            $sheet->mergeCells('CX5:DA5');
                            $sheet->mergeCells('DC5:DF5');
                            $sheet->mergeCells('DH5:DK5');
                            $sheet->mergeCells('DM5:DP5');
                            $sheet->mergeCells('DR5:DU5');
                            $sheet->mergeCells('DW5:DZ5');
                            $sheet->mergeCells('EB5:EE5');
                            $sheet->mergeCells('EG5:EJ5');
                            $sheet->mergeCells('EL5:EO5');
                            $sheet->mergeCells('EQ5:ET5');
                            $sheet->mergeCells('EV5:EY5');
                            $sheet->mergeCells('FA5:FD5');
                            $sheet->mergeCells('FF5:FI5');
                            $sheet->mergeCells('FK5:FN5');
                            $sheet->mergeCells('FP5:FS5');

                            $sheet->setBorder('A5:F5', 'thin');

                            $sheet->setBorder('H5:J5', 'thin');
                            $sheet->setBorder('L5:O5', 'thin');
                            $sheet->setBorder('Q5:T5', 'thin');
                            $sheet->setBorder('V5:Y5', 'thin');
                            $sheet->setBorder('AA5:AD5', 'thin');
                            $sheet->setBorder('AF5:AI5', 'thin');
                            $sheet->setBorder('AK5:AN5', 'thin');
                            $sheet->setBorder('AP5:AS5', 'thin');
                            $sheet->setBorder('AU5:AX5', 'thin');
                            $sheet->setBorder('AZ5:BC5', 'thin');
                            $sheet->setBorder('BE5:BH5', 'thin');
                            $sheet->setBorder('BJ5:BM5', 'thin');
                            $sheet->setBorder('BO5:BR5', 'thin');
                            $sheet->setBorder('BT5:BW5', 'thin');
                            $sheet->setBorder('BY5:CB5', 'thin');
                            $sheet->setBorder('CD5:CG5', 'thin');
                            $sheet->setBorder('CI5:CL5', 'thin');
                            $sheet->setBorder('CN5:CQ5', 'thin');
                            $sheet->setBorder('CS5:CV5', 'thin');
                            $sheet->setBorder('CX5:DA5', 'thin');
                            $sheet->setBorder('DC5:DF5', 'thin');
                            $sheet->setBorder('DH5:DK5', 'thin');
                            $sheet->setBorder('DM5:DP5', 'thin');
                            $sheet->setBorder('DR5:DU5', 'thin');
                            $sheet->setBorder('DW5:DZ5', 'thin');
                            $sheet->setBorder('EB5:EE5', 'thin');
                            $sheet->setBorder('EG5:EJ5', 'thin');
                            $sheet->setBorder('EL5:EO5', 'thin');
                            $sheet->setBorder('EQ5:ET5', 'thin');
                            $sheet->setBorder('EV5:EY5', 'thin');
                            $sheet->setBorder('FA5:FD5', 'thin');
                            $sheet->setBorder('FF5:FI5', 'thin');
                            $sheet->setBorder('FK5:FN5', 'thin');
                            $sheet->setBorder('FP5:FS5', 'thin');

                             $sheet->setBorder('A6:F6', 'thin');

                            $sheet->setBorder('H6:J6', 'thin');
                            $sheet->setBorder('L6:O6', 'thin');
                            $sheet->setBorder('Q6:T6', 'thin');
                            $sheet->setBorder('V6:Y6', 'thin');
                            $sheet->setBorder('AA6:AD6', 'thin');
                            $sheet->setBorder('AF6:AI6', 'thin');
                            $sheet->setBorder('AK6:AN6', 'thin');
                            $sheet->setBorder('AP6:AS6', 'thin');
                            $sheet->setBorder('AU6:AX6', 'thin');
                            $sheet->setBorder('AZ6:BC6', 'thin');
                            $sheet->setBorder('BE6:BH6', 'thin');
                            $sheet->setBorder('BJ6:BM6', 'thin');
                            $sheet->setBorder('BO6:BR6', 'thin');
                            $sheet->setBorder('BT6:BW6', 'thin');
                            $sheet->setBorder('BY6:CB6', 'thin');
                            $sheet->setBorder('CD6:CG6', 'thin');
                            $sheet->setBorder('CI6:CL6', 'thin');
                            $sheet->setBorder('CN6:CQ6', 'thin');
                            $sheet->setBorder('CS6:CV6', 'thin');
                            $sheet->setBorder('CX6:DA6', 'thin');
                            $sheet->setBorder('DC6:DF6', 'thin');
                            $sheet->setBorder('DH6:DK6', 'thin');
                            $sheet->setBorder('DM6:DP6', 'thin');
                            $sheet->setBorder('DR6:DU6', 'thin');
                            $sheet->setBorder('DW6:DZ6', 'thin');
                            $sheet->setBorder('EB6:EE6', 'thin');
                            $sheet->setBorder('EG6:EJ6', 'thin');
                            $sheet->setBorder('EL6:EO6', 'thin');
                            $sheet->setBorder('EQ6:ET6', 'thin');
                            $sheet->setBorder('EV6:EY6', 'thin');
                            $sheet->setBorder('FA6:FD6', 'thin');
                            $sheet->setBorder('FF6:FI6', 'thin');
                            $sheet->setBorder('FK6:FN6', 'thin');
                            $sheet->setBorder('FP6:FS6', 'thin');

                             $sheet->cells('A5:FS5', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });

                              $sheet->cells('A6:FS6', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });


            				$sheet->row(1, array("PANDORA - ".$branch->branch));
                            $sheet->row(2, array("DETAILED DAILY CREDIT CARD TRANSACTIONS - "));
                            $sheet->row(3, array("FOR THE PERIOD ".$date_from." to ".$date_to));

                            $sheet->row(5,array('' ,'SALES INV','REF NO.','','ATM/','CREDIT CARD',' ','ATM/EPS',' ', ' ',' ', 'STRAIGHT TRANS OTHER THAN JCB/AMEX', ' ', ' ', ' ',' ','JCB/AMEX STRAIGHT TRANS',' ',' ',' ',' ','BDO 3 MONTHS', ' ' , ' ', ' ', ' ','BDO 6 MONTHS',' ',' ',' ',' ','BDO 12 MONTHS',' ','',' ',' ','BPI 3 MONTHS',' ',' ',' ',' ','BPI 6 MONTHS',' ',' ',' ',' ','BPI 12 MONTHS',' ','','','', 'CITIBANK 3 MONTHS',' ', ' ',' ',' ','CITIBANK 6 MONTHS',' ',' ',' ',' ','CITIBANK 12 MONTHS',' ','','','','HSBC 3 MONTHS',' ',' ',' ',' ','HSBC 6 MONTHS',' ',' ',' ',' ','HSBC 12 MONTHS',' ','','','','EAST WEST 3 MONTHS',' ',' ',' ',' ','EAST WEST  6 MONTHS',' ',' ',' ',' ','EAST WEST  12 MONTHS',' ','','','','METROBANK 3 MONTHS',' ',' ',' ',' ','METROBANK 6 MONTHS',' ',' ',' ',' ','METROBANK 12 MONTHS',' ','','','','DINERS/SECURITY BANK 3 MONTHS', ' ',' ',' ',' ',' DINERS/SECURITY BANK 6 MONTHS',' ',' ',' ',' ','DINERS/SECURITY BANK 12 MONTHS' , ' ' , '' , '' , '' ,'PNB 3 MONTHS', ' ',' ',' ',' ','PNB 6 MONTHS' , ' ', ' ',' ',' ','PNB 12 MONTHS' , ' ',' ',' ',' ','RCBC 3 MONTHS', ' ',' ',' ',' ','RCBC 6 MONTHS',' ',' ',' ',' ','RCBC 12 MONTHS' ,' ' , ' ' ,'','' ,'UNION BANK 3 MONTHS',' ',' ',' ',' ','UNION BANK 6 MONTHS',' ',' ',' ',' ','UNION BANK 12 MONTHS',' ',' ',' '));





                            $sheet->row(6,array('DATE','NUMBER','','CUSTOMER NAME','EPS','TRANSACTIONS',' ','AMOUNT','MERCHANT DISCT','NET AMOUNT',' ','AMOUNT','MERCHANT DISCT','EWT','NET AMOUNT',' ','AMOUNT','MERCHANT DISCT','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','MSUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',));

                            $cnt = 8;
                             $index = 0;
                               $prev = 0;

                           
                             foreach ($sales as $data) {




                                $bdo_amount_3 = 0.0;
                                $bdo_amount_6 = 0.0;
                                $bdo_amount_12 = 0.0;

                                $bpi_amount_3 = 0.0;
                                $bpi_amount_6 = 0.0;
                                $bpi_amount_12 = 0.0;

                                $citibank_amount_3 = 0.0;
                                $citibank_amount_6 = 0.0;
                                $citibank_amount_12 = 0.0;


                                $hsbc_amount_3 = 0.0;
                                $hsbc_amount_6 = 0.0;
                                $hsbc_amount_12 = 0.0;

                                $eastwest_amount_3 = 0.0;
                                $eastwest_amount_6 = 0.0;
                                $eastwest_amount_12 = 0.0;


                                $metrobank_amount_3 = 0.0;
                                $metrobank_amount_6 = 0.0;
                                $metrobank_amount_12 = 0.0;

                                $dinner_amount_3 = 0.0;
                                $dinner_amount_6 = 0.0;
                                $dinner_amount_12 = 0.0;

                                $pnb_amount_3 = 0.0;
                                $pnb_amount_6 = 0.0;
                                $pnb_amount_12  = 0.0;

                                $rcbc_amount_3 = 0.0;
                                $rcbc_amount_6 = 0.0;
                                $rcbc_amount_12 = 0.0;

                                $unionbank_amount_3 = 0.0;
                                $unionbank_amount_6 = 0.0;
                                $unionbank_amount_12 = 0.0;



                                $bdo_subsidy_3 = 0.0;
                                $bdo_subsidy_6 = 0.0;
                                $bdo_amount_12 = 0.0;

                                $bpi_subsidy_3 = 0.0;
                                $bpi_subsidy_6 = 0.0;
                                $bpi_subsidy_12 = 0.0;

                                $citibank_subsidy_3 = 0.0;
                                $citibank_subsidy_6 = 0.0;
                                $citibank_subsidy_12 = 0.0;


                                $hsbc_subsidy_3 = 0.0;
                                $hsbc_subsidy_6 = 0.0;
                                $hsbc_subsidy_12 = 0.0;

                                $eastwest_subsidy_3 = 0.0;
                                $eastwest_subsidy_6 = 0.0;
                                $eastwest_subsidy_12 = 0.0;


                                $metrobank_subsidy_3 = 0.0;
                                $metrobank_subsidy_6 = 0.0;
                                $metrobank_subsidy_12 = 0.0;

                                $dinner_subsidy_3 = 0.0;
                                $dinner_subsidy_6 = 0.0;
                                $dinner_subsidy_12 = 0.0;

                                $pnb_subsidy_3 = 0.0;
                                $pnb_subsidy_6 = 0.0;
                                $pnb_subsidy_12  = 0.0;

                                $rcbc_subsidy_3 = 0.0;
                                $rcbc_subsidy_6 = 0.0;
                                $rcbc_subsidy_12 = 0.0;

                                $unionbank_subsidy_3 = 0.0;
                                $unionbank_subsidy_6 = 0.0;
                                $unionbank_subsidy_12 = 0.0;





                                  $bdo_amount_3  = $data->bdo_3;
                               $bdo_amount_6  = $data->bdo_6;
                               $bdo_amount_12 = $data->bdo_12;

                               $bpi_amount_3 = $data->bpi_3;
                               $bpi_amount_6 = $data->bpi_6;
                               $bpi_amount_12 = $data->bpi_12;

                               $citibank_amount_3 = $data->citibank_3;
                               $citibank_amount_6 = $data->citibank_6;
                               $citibank_amount_12 = $data->citibank_12;


                               $hsbc_amount_3 = $data->hsbc_3;
                               $hsbc_amount_6 = $data->hsbc_6;
                               $hsbc_amount_12 = $data->hsbc_12;

                               $eastwest_amount_3 = $data->eastwest_3;
                               $eastwest_amount_6 = $data->eastwest_6;
                               $eastwest_amount_12 = $data->eastwest_12;


                               $metrobank_amount_3 = $data->metrobank_3;
                               $metrobank_amount_6 = $data->metrobank_6;
                               $metrobank_amount_12 = $data->metrobank_12;

                               $dinner_amount_3 = $data->diners_3;
                               $dinner_amount_6 = $data->diners_6;
                               $dinner_amount_12 = $data->diners_12;

                               $pnb_amount_3 = $data->pnb_3;
                               $pnb_amount_6 = $data->pnb_6;    
                               $pnb_amount_12 = $data->pnb_12;

                               $rcbc_amount_3 = $data->rcbc_3;
                               $rcbc_amount_6 = $data->rcbc_6;
                               $rcbc_amount_12 = $data->rcbc_12;


                               $unionbank_amount_3 = $data->unionbank_3;
                               $unionbank_amount_6 = $data->unionbank_6;
                               $unionbank_amount_12 = $data->unionbank_12;


                               $bdo_subsidy_3 = $bdo_amount_3 * 0.025;
                               $bdo_subsidy_6 = $bdo_amount_6 * 0.0475;
                               $bdo_subsidy_12 = $bdo_amount_12 *  0.095;

                               $bpi_subsidy_3 = $bpi_amount_3 *  0.0275;
                               $bpi_subsidy_6 = $bpi_amount_6 * 0.05;
                               $bpi_subsidy_12 = $bpi_amount_12 *  0.095;


                               $citibank_subsidy_3 = $citibank_amount_3 *  0.025;
                               $citibank_subsidy_6 = $citibank_amount_6 * 0.05;
                               $citibank_subsidy_12 = $citibank_amount_12 *  0.09;


                               $hsbc_subsidy_3 = $hsbc_amount_3 *  0.025;
                               $hsbc_subsidy_6 = $hsbc_amount_6 * 0.045;
                               $hsbc_subsidy_12 = $hsbc_amount_12 *  0.075;


                               $eastwest_subsidy_3 = $eastwest_amount_3 *  0.025;
                               $eastwest_subsidy_6 = $eastwest_amount_6 * 0.0475;
                               $eastwest_subsidy_12 = $eastwest_amount_12 *  0.095;

                                $metrobank_subsidy_3 = $metrobank_amount_3 *  0.03;
                                 $metrobank_subsidy_6 = $metrobank_amount_6 * 0.0475;
                                 $metrobank_subsidy_12 = $metrobank_amount_12 *  0.095;


                                 $dinner_subsidy_3 = $dinner_amount_3 *  0.025;
                                 $dinner_subsidy_6 = $dinner_amount_6 * 0.0475;
                                 $dinner_subsidy_12 = 0.00;


                                 $pnb_subsidy_3 = $pnb_amount_3 *  0.03;
                                 $pnb_subsidy_6 = $pnb_amount_6 * 0.0475;
                                 $pnb_subsidy_12 = $pnb_amount_12 * 0.095;

                                 $rcbc_subsidy_3 = $rcbc_amount_3 *  0.03;
                                 $rcbc_subsidy_6 = $rcbc_amount_6 * 0.06;
                                 $rcbc_subsidy_12 = 0.00;

                                 
                                 $unionbank_subsidy_3 = $unionbank_amount_3 *  0.025;
                                 $unionbank_subsidy_6 = $unionbank_amount_6 * 0.05;
                                 $unionbank_subsidy_12 = $unionbank_amount_12 * 0.1;
                                 $first_date = date('d-M',strtotime($data->local_time));

                                   if($first_date != $last_date && $index > 0) {
                                   
                                    $_atm_total = 0.00;
                                    $_credit_card_total = 0.00;

                                    $_atm_amount_total = 0.00;
                                    $_atm_merch_total = 0.00;
                                    $_atm_net_total =0.00;

                                    $_str_amount_total = 0.00;
                                    $_str_merch_total = 0.00;
                                    $_str_ewt_total =0.00;
                                    $_str_net_total =0.00;

                                    $_jcb_amount_total = 0.00;
                                    $_jcb_merch_total = 0.00;
                                    $_jcb_ewt_total =0.00;
                                    $_jcb_net_total =0.00;

                                    $_bdo3_amount_total = 0.00;
                                    $_bdo3_merch_total = 0.00;
                                    $_bdo3_ewt_total =0.00;
                                    $_bdo3_net_total =0.00;

                                    $_bdo6_amount_total = 0.00;
                                    $_bdo6_merch_total = 0.00;
                                    $_bdo6_ewt_total =0.00;
                                    $_bdo6_net_total =0.00;

                                    $_bdo12_amount_total = 0.00;
                                    $_bdo12_merch_total = 0.00;
                                    $_bdo12_ewt_total =0.00;
                                    $_bdo12_net_total =0.00;

                                    $_bpi3_amount_total = 0.00;
                                    $_bpi3_merch_total = 0.00;
                                    $_bpi3_ewt_total =0.00;
                                    $_bdo3_net_total =0.00;

                                    $_bpi6_amount_total = 0.00;
                                    $_bpi6_merch_total = 0.00;
                                    $_bpi6_ewt_total =0.00;
                                    $_bpi6_net_total =0.00;

                                    $_bpi12_amount_total = 0.00;
                                    $_bpi12_merch_total = 0.00;
                                    $_bpi12_ewt_total =0.00;
                                    $_bpi12_net_total =0.00;


                                    $_citibank3_amount_total = 0.00;
                                    $_citibank3_merch_total = 0.00;
                                    $_citibank3_ewt_total =0.00;
                                    $_citibank3_net_total =0.00;

                                    $_citibank6_amount_total = 0.00;
                                    $_citibank6_merch_total = 0.00;
                                    $_citibank6_ewt_total =0.00;
                                    $_citibank6_net_total =0.00;

                                    $_citibank12_amount_total = 0.00;
                                    $_citibank12_merch_total = 0.00;
                                    $_citibank12_ewt_total =0.00;
                                    $_citibank12_net_total =0.00;

                                    $_hsbc3_amount_total = 0.00;
                                    $_hsbc3_merch_total = 0.00;
                                    $_hsbc3_ewt_total =0.00;
                                    $_hsbc3_net_total =0.00;

                                    $_hsbc6_amount_total = 0.00;
                                    $_hsbc6_merch_total = 0.00;
                                    $_hsbc6_ewt_total =0.00;
                                    $_hsbc6_net_total =0.00;

                                    $_hsbc12_amount_total = 0.00;
                                    $_hsbc12_merch_total = 0.00;
                                    $_hsbc12_ewt_total =0.00;
                                    $_hsbc12_net_total =0.00;


                                    $_east3_amount_total = 0.00;
                                    $_east3_merch_total = 0.00;
                                    $_east3_ewt_total =0.00;
                                    $_east3_net_total =0.00;

                                    $_east6_amount_total = 0.00;
                                    $_east6_merch_total = 0.00;
                                    $_east6_ewt_total =0.00;
                                    $_east6_net_total =0.00;

                                    $_east12_amount_total = 0.00;
                                    $_east12_merch_total = 0.00;
                                    $_east12_ewt_total =0.00;
                                    $_east12_net_total =0.00;

                                    $_metro3_amount_total = 0.00;
                                    $_metro3_merch_total = 0.00;
                                    $_metro3_ewt_total =0.00;
                                    $_metro3_net_total =0.00;

                                    $_metro6_amount_total = 0.00;
                                    $_metro6_merch_total = 0.00;
                                    $_metro6_ewt_total =0.00;
                                    $_metro6_net_total =0.00;

                                    $_metro12_amount_total = 0.00;
                                    $_metro12_merch_total = 0.00;
                                    $_metro12_ewt_total =0.00;
                                    $_metro12_net_total =0.00;


                                    $_diner3_amount_total = 0.00;
                                    $_diner3_merch_total = 0.00;
                                    $_diner3_ewt_total =0.00;
                                    $_diner3_net_total =0.00;

                                    $_diner6_amount_total = 0.00;
                                    $_diner6_merch_total = 0.00;
                                    $_diner6_ewt_total =0.00;
                                    $_diner6_net_total =0.00;

                                    $_diner12_amount_total = 0.00;
                                    $_diner12_merch_total = 0.00;
                                    $_diner12_ewt_total =0.00;
                                    $_diner12_net_total =0.00;


                                    $_pnb3_amount_total = 0.00;
                                    $_pnb3_merch_total = 0.00;
                                    $_pnb3_ewt_total =0.00;
                                    $_pnb3_net_total =0.00;

                                    $_pnb6_amount_total = 0.00;
                                    $_pnb6_merch_total = 0.00;
                                    $_pnb6_ewt_total =0.00;
                                    $_pnb6_net_total =0.00;

                                    $_pnb12_amount_total = 0.00;
                                    $_pnb12_merch_total = 0.00;
                                    $_pnb12_ewt_total =0.00;
                                    $_pnb12_net_total =0.00;


                                    $_rcbc3_amount_total = 0.00;
                                    $_rcbc3_merch_total = 0.00;
                                    $_rcbc3_ewt_total =0.00;
                                    $_rcbc3_net_total =0.00;

                                    $_rcbc6_amount_total = 0.00;
                                    $_rcbc6_merch_total = 0.00;
                                    $_rcbc6_ewt_total =0.00;
                                    $_rcbc6_net_total =0.00;

                                    $_rcbc12_amount_total = 0.00;
                                    $_rcbc12_merch_total = 0.00;
                                    $_rcbc12_ewt_total =0.00;
                                    $_rcbc12_net_total =0.00;

                                    $_union3_amount_total = 0.00;
                                    $_union3_merch_total = 0.00;
                                    $_union3_ewt_total =0.00;
                                    $_union3_net_total =0.00;

                                    $_union6_amount_total = 0.00;
                                    $_union6_merch_total = 0.00;
                                    $_union6_ewt_total =0.00;
                                    $_union6_net_total =0.00;

                                    $_union12_amount_total = 0.00;
                                    $_union12_merch_total = 0.00;
                                    $_union12_ewt_total =0.00;
                                    $_union12_net_total =0.00;








                                    if($index == 2) {



                                    $_atm_total = sprintf("=SUM(E%s:E%s)",8,$cnt-1);
                                    $_credit_card_total = sprintf("=SUM(F%s:F%s)",8,$cnt-1);

                                    $_atm_amount_total = sprintf("=SUM(H%s:H%s)",8,$cnt-1);
                                    $_atm_merch_total = sprintf("=SUM(I%s:I%s)",8,$cnt-1);
                                    $_atm_net_total = sprintf("=SUM(J%s:J%s)",8,$cnt-1);

                                    $_str_amount_total = sprintf("=SUM(L%s:L%s)",8,$cnt-1);
                                    $_str_merch_total = sprintf("=SUM(M%s:M%s)",8,$cnt-1);
                                    $_str_ewt_total = sprintf("=SUM(N%s:N%s)",8,$cnt-1);
                                    $_str_net_total =sprintf("=SUM(O%s:O%s)",8,$cnt-1);

                                    $_jcb_amount_total = sprintf("=SUM(Q%s:Q%s)",8,$cnt-1);
                                    $_jcb_merch_total = sprintf("=SUM(R%s:R%s)",8,$cnt-1);
                                    $_jcb_ewt_total = sprintf("=SUM(S%s:S%s)",8,$cnt-1);
                                    $_jcb_net_total =sprintf("=SUM(T%s:T%s)",8,$cnt-1);

                                    $_bdo3_amount_total = sprintf("=SUM(V%s:V%s)",8,$cnt-1);
                                    $_bdo3_merch_total = sprintf("=SUM(W%s:W%s)",8,$cnt-1);
                                    $_bdo3_ewt_total = sprintf("=SUM(X%s:X%s)",8,$cnt-1);
                                    $_bdo3_net_total = sprintf("=SUM(Y%s:Y%s)",8,$cnt-1);

                                    $_bdo6_amount_total = sprintf("=SUM(AA%s:AA%s)",8,$cnt-1);
                                    $_bdo6_merch_total = sprintf("=SUM(AB%s:AB%s)",8,$cnt-1);
                                    $_bdo6_ewt_total = sprintf("=SUM(AC%s:AC%s)",8,$cnt-1);
                                    $_bdo6_net_total = sprintf("=SUM(AD%s:AD%s)",8,$cnt-1);

                                    $_bdo12_amount_total = sprintf("=SUM(AF%s:AF%s)",8,$cnt-1);
                                    $_bdo12_merch_total = sprintf("=SUM(AG%s:AG%s)",8,$cnt-1);
                                    $_bdo12_ewt_total = sprintf("=SUM(AH%s:AH%s)",8,$cnt-1);
                                    $_bdo12_net_total = sprintf("=SUM(AI%s:AI%s)",8,$cnt-1);

                                    $_bpi3_amount_total = sprintf("=SUM(AK%s:AK%s)",8,$cnt-1);
                                    $_bpi3_merch_total = sprintf("=SUM(AL%s:AL%s)",8,$cnt-1);
                                    $_bpi3_ewt_total = sprintf("=SUM(AM%s:AM%s)",8,$cnt-1);
                                    $_bdo3_net_total = sprintf("=SUM(AN%s:AN%s)",8,$cnt-1);

                                    $_bpi6_amount_total =  sprintf("=SUM(AP%s:AP%s)",8,$cnt-1);
                                    $_bpi6_merch_total = sprintf("=SUM(AQ%s:AQ%s)",8,$cnt-1);
                                    $_bpi6_ewt_total = sprintf("=SUM(AR%s:AR%s)",8,$cnt-1);
                                    $_bpi6_net_total = sprintf("=SUM(AS%s:AS%s)",8,$cnt-1);

                                    $_bpi12_amount_total = sprintf("=SUM(AU%s:AU%s)",8,$cnt-1);
                                    $_bpi12_merch_total = sprintf("=SUM(AV%s:AV%s)",8,$cnt-1);
                                    $_bpi12_ewt_total = sprintf("=SUM(AW%s:AW%s)",8,$cnt-1);
                                    $_bpi12_net_total = sprintf("=SUM(AX%s:AX%s)",8,$cnt-1);


                                    $_citibank3_amount_total = sprintf("=SUM(AZ%s:AZ%s)",8,$cnt-1);
                                    $_citibank3_merch_total = sprintf("=SUM(BA%s:BA%s)",8,$cnt-1);
                                    $_citibank3_ewt_total = sprintf("=SUM(BB%s:BB%s)",8,$cnt-1);
                                    $_citibank3_net_total = sprintf("=SUM(BC%s:BC%s)",8,$cnt-1);

                                    $_citibank6_amount_total = sprintf("=SUM(BE%s:BE%s)",8,$cnt-1);
                                    $_citibank6_merch_total = sprintf("=SUM(BF%s:BF%s)",8,$cnt-1);
                                    $_citibank6_ewt_total = sprintf("=SUM(BG%s:BG%s)",8,$cnt-1);
                                    $_citibank6_net_total = sprintf("=SUM(BH%s:BH%s)",8,$cnt-1);

                                    $_citibank12_amount_total = sprintf("=SUM(BJ%s:BJ%s)",8,$cnt-1);
                                    $_citibank12_merch_total = sprintf("=SUM(BK%s:BK%s)",8,$cnt-1);
                                    $_citibank12_ewt_total = sprintf("=SUM(BL%s:BL%s)",8,$cnt-1);
                                    $_citibank12_net_total = sprintf("=SUM(BM%s:BM%s)",8,$cnt-1);

                                    $_hsbc3_amount_total = sprintf("=SUM(BO%s:BO%s)",8,$cnt-1);
                                    $_hsbc3_merch_total = sprintf("=SUM(BP%s:BP%s)",8,$cnt-1);
                                    $_hsbc3_ewt_total = sprintf("=SUM(BQ%s:BQ%s)",8,$cnt-1);
                                    $_hsbc3_net_total = sprintf("=SUM(BR%s:BR%s)",8,$cnt-1);

                                    $_hsbc6_amount_total = sprintf("=SUM(BT%s:BT%s)",8,$cnt-1);
                                    $_hsbc6_merch_total = sprintf("=SUM(BU%s:BU%s)",8,$cnt-1);
                                    $_hsbc6_ewt_total = sprintf("=SUM(BV%s:BV%s)",8,$cnt-1);
                                    $_hsbc6_net_total = sprintf("=SUM(BW%s:BW%s)",8,$cnt-1);

                                    $_hsbc12_amount_total = sprintf("=SUM(BY%s:BY%s)",8,$cnt-1);
                                    $_hsbc12_merch_total = sprintf("=SUM(BZ%s:BZ%s)",8,$cnt-1);
                                    $_hsbc12_ewt_total = sprintf("=SUM(CA%s:CA%s)",8,$cnt-1);
                                    $_hsbc12_net_total = sprintf("=SUM(CB%s:CB%s)",8,$cnt-1);


                                    $_east3_amount_total = sprintf("=SUM(CD%s:CD%s)",8,$cnt-1);
                                    $_east3_merch_total = sprintf("=SUM(CE%s:CE%s)",8,$cnt-1);
                                    $_east3_ewt_total = sprintf("=SUM(CF%s:CF%s)",8,$cnt-1);
                                    $_east3_net_total = sprintf("=SUM(CG%s:CG%s)",8,$cnt-1);

                                    $_east6_amount_total = sprintf("=SUM(CI%s:CI%s)",8,$cnt-1);
                                    $_east6_merch_total = sprintf("=SUM(CJ%s:CJ%s)",8,$cnt-1);
                                    $_east6_ewt_total = sprintf("=SUM(CK%s:CK%s)",8,$cnt-1);
                                    $_east6_net_total = sprintf("=SUM(CL%s:CL%s)",8,$cnt-1);

                                    $_east12_amount_total = sprintf("=SUM(CN%s:CN%s)",8,$cnt-1);
                                    $_east12_merch_total = sprintf("=SUM(CO%s:CO%s)",8,$cnt-1);
                                    $_east12_ewt_total = sprintf("=SUM(CP%s:CP%s)",8,$cnt-1);
                                    $_east12_net_total = sprintf("=SUM(CQ%s:CQ%s)",8,$cnt-1);

                                    $_metro3_amount_total = sprintf("=SUM(CS%s:CS%s)",8,$cnt-1);
                                    $_metro3_merch_total = sprintf("=SUM(CT%s:CT%s)",8,$cnt-1);
                                    $_metro3_ewt_total = sprintf("=SUM(CU%s:CU%s)",8,$cnt-1);
                                    $_metro3_net_total = sprintf("=SUM(CV%s:CV%s)",8,$cnt-1);

                                    $_metro6_amount_total = sprintf("=SUM(CX%s:CX%s)",8,$cnt-1);
                                    $_metro6_merch_total = sprintf("=SUM(CY%s:CY%s)",8,$cnt-1);
                                    $_metro6_ewt_total = sprintf("=SUM(CZ%s:CZ%s)",8,$cnt-1);
                                    $_metro6_net_total = sprintf("=SUM(DA%s:DA%s)",8,$cnt-1);

                                    $_metro12_amount_total = sprintf("=SUM(DC%s:DC%s)",8,$cnt-1);
                                    $_metro12_merch_total = sprintf("=SUM(DD%s:DD%s)",8,$cnt-1);
                                    $_metro12_ewt_total = sprintf("=SUM(DE%s:DE%s)",8,$cnt-1);
                                    $_metro12_net_total = sprintf("=SUM(DF%s:DF%s)",8,$cnt-1);


                                    $_diner3_amount_total = sprintf("=SUM(DH%s:DH%s)",8,$cnt-1);
                                    $_diner3_merch_total = sprintf("=SUM(DI%s:DI%s)",8,$cnt-1);
                                    $_diner3_ewt_total = sprintf("=SUM(DJ%s:DJ%s)",8,$cnt-1);
                                    $_diner3_net_total = sprintf("=SUM(DK%s:DK%s)",8,$cnt-1);

                                    $_diner6_amount_total = sprintf("=SUM(DM%s:DM%s)",8,$cnt-1);
                                    $_diner6_merch_total = sprintf("=SUM(DN%s:DN%s)",8,$cnt-1);
                                    $_diner6_ewt_total = sprintf("=SUM(DO%s:DO%s)",8,$cnt-1);
                                    $_diner6_net_total = sprintf("=SUM(DP%s:DP%s)",8,$cnt-1);

                                    $_diner12_amount_total = sprintf("=SUM(DR%s:DR%s)",8,$cnt-1);
                                    $_diner12_merch_total = sprintf("=SUM(DS%s:Ds%s)",8,$cnt-1);
                                    $_diner12_ewt_total = sprintf("=SUM(DT%s:DT%s)",8,$cnt-1);
                                    $_diner12_net_total = sprintf("=SUM(DU%s:DU%s)",8,$cnt-1);


                                    $_pnb3_amount_total = sprintf("=SUM(DW%s:DW%s)",8,$cnt-1);
                                    $_pnb3_merch_total = sprintf("=SUM(DX%s:DX%s)",8,$cnt-1);
                                    $_pnb3_ewt_total =sprintf("=SUM(DY%s:DY%s)",8,$cnt-1);
                                    $_pnb3_net_total =sprintf("=SUM(DZ%s:DZ%s)",8,$cnt-1);

                                    $_pnb6_amount_total = sprintf("=SUM(EB%s:EB%s)",8,$cnt-1);
                                    $_pnb6_merch_total = sprintf("=SUM(EC%s:EC%s)",8,$cnt-1);
                                    $_pnb6_ewt_total = sprintf("=SUM(ED%s:ED%s)",8,$cnt-1);
                                    $_pnb6_net_total = sprintf("=SUM(EE%s:EE%s)",8,$cnt-1);

                                    $_pnb12_amount_total = sprintf("=SUM(EG%s:EG%s)",8,$cnt-1);
                                    $_pnb12_merch_total = sprintf("=SUM(EH%s:EH%s)",8,$cnt-1);
                                    $_pnb12_ewt_total = sprintf("=SUM(EI%s:EI%s)",8,$cnt-1);
                                    $_pnb12_net_total = sprintf("=SUM(EJ%s:EJ%s)",8,$cnt-1);


                                    $_rcbc3_amount_total = sprintf("=SUM(EL%s:EL%s)",8,$cnt-1);
                                    $_rcbc3_merch_total = sprintf("=SUM(EM%s:EM%s)",8,$cnt-1);
                                    $_rcbc3_ewt_total = sprintf("=SUM(EN%s:EN%s)",8,$cnt-1);
                                    $_rcbc3_net_total = sprintf("=SUM(EO%s:EO%s)",8,$cnt-1);

                                    $_rcbc6_amount_total = sprintf("=SUM(EQ%s:EQ%s)",8,$cnt-1);
                                    $_rcbc6_merch_total = sprintf("=SUM(ER%s:ER%s)",8,$cnt-1);
                                    $_rcbc6_ewt_total = sprintf("=SUM(ES%s:ES%s)",8,$cnt-1);
                                    $_rcbc6_net_total = sprintf("=SUM(ET%s:ET%s)",8,$cnt-1);

                                    $_rcbc12_amount_total = sprintf("=SUM(EV%s:EV%s)",8,$cnt-1);
                                    $_rcbc12_merch_total = sprintf("=SUM(EW%s:EW%s)",8,$cnt-1);
                                    $_rcbc12_ewt_total = sprintf("=SUM(EX%s:EX%s)",8,$cnt-1);
                                    $_rcbc12_net_total = sprintf("=SUM(EY%s:EY%s)",8,$cnt-1);

                                    $_union3_amount_total = sprintf("=SUM(FA%s:FA%s)",8,$cnt-1);
                                    $_union3_merch_total = sprintf("=SUM(FB%s:FB%s)",8,$cnt-1);
                                    $_union3_ewt_total = sprintf("=SUM(FC%s:FC%s)",8,$cnt-1);
                                    $_union3_net_total = sprintf("=SUM(FD%s:FD%s)",8,$cnt-1);

                                    $_union6_amount_total = printf("=SUM(FF%s:FF%s)",8,$cnt-1);
                                    $_union6_merch_total = sprintf("=SUM(FG%s:FG%s)",8,$cnt-1);
                                    $_union6_ewt_total = sprintf("=SUM(FH%s:FH%s)",8,$cnt-1);
                                    $_union6_net_total = sprintf("=SUM(FI%s:FI%s)",8,$cnt-1);

                                    $_union12_amount_total = sprintf("=SUM(FK%s:FK%s)",8,$cnt-1);
                                    $_union12_merch_total = sprintf("=SUM(FL%s:FL%s)",8,$cnt-1);
                                    $_union12_ewt_total = sprintf("=SUM(FM%s:FM%s)",8,$cnt-1);
                                    $_union12_net_total = sprintf("=SUM(FN%s:FN%s)",8,$cnt-1);










                                    }
                                    else {

                                     

                                    $_atm_total = sprintf("=SUM(E%s:E%s)",$prev+1,$cnt-1);
                                    $_credit_card_total = sprintf("=SUM(F%s:F%s)",$prev+1,$cnt-1);

                                    $_atm_amount_total = sprintf("=SUM(H%s:H%s)",$prev+1,$cnt-1);
                                    $_atm_merch_total = sprintf("=SUM(I%s:I%s)",$prev+1,$cnt-1);
                                    $_atm_net_total = sprintf("=SUM(J%s:J%s)",$prev+1,$cnt-1);

                                    $_str_amount_total = sprintf("=SUM(L%s:L%s)",$prev+1,$cnt-1);
                                    $_str_merch_total = sprintf("=SUM(M%s:M%s)",$prev+1,$cnt-1);
                                    $_str_ewt_total = sprintf("=SUM(N%s:N%s)",$prev+1,$cnt-1);
                                    $_str_net_total =sprintf("=SUM(O%s:O%s)",$prev+1,$cnt-1);

                                    $_jcb_amount_total = sprintf("=SUM(Q%s:Q%s)",$prev+1,$cnt-1);
                                    $_jcb_merch_total = sprintf("=SUM(R%s:R%s)",$prev+1,$cnt-1);
                                    $_jcb_ewt_total = sprintf("=SUM(S%s:S%s)",$prev+1,$cnt-1);
                                    $_jcb_net_total =sprintf("=SUM(T%s:T%s)",$prev+1,$cnt-1);

                                    $_bdo3_amount_total = sprintf("=SUM(V%s:V%s)",$prev+1,$cnt-1);
                                    $_bdo3_merch_total = sprintf("=SUM(W%s:W%s)",$prev+1,$cnt-1);
                                    $_bdo3_ewt_total = sprintf("=SUM(X%s:X%s)",$prev+1,$cnt-1);
                                    $_bdo3_net_total = sprintf("=SUM(Y%s:Y%s)",$prev+1,$cnt-1);

                                    $_bdo6_amount_total = sprintf("=SUM(AA%s:AA%s)",$prev+1,$cnt-1);
                                    $_bdo6_merch_total = sprintf("=SUM(AB%s:AB%s)",$prev+1,$cnt-1);
                                    $_bdo6_ewt_total = sprintf("=SUM(AC%s:AC%s)",$prev+1,$cnt-1);
                                    $_bdo6_net_total = sprintf("=SUM(AD%s:AD%s)",$prev+1,$cnt-1);

                                    $_bdo12_amount_total = sprintf("=SUM(AF%s:AF%s)",$prev+1,$cnt-1);
                                    $_bdo12_merch_total = sprintf("=SUM(AG%s:AG%s)",$prev+1,$cnt-1);
                                    $_bdo12_ewt_total = sprintf("=SUM(AH%s:AH%s)",$prev+1,$cnt-1);
                                    $_bdo12_net_total = sprintf("=SUM(AI%s:AI%s)",$prev+1,$cnt-1);

                                    $_bpi3_amount_total = sprintf("=SUM(AK%s:AK%s)",$prev+1,$cnt-1);
                                    $_bpi3_merch_total = sprintf("=SUM(AL%s:AL%s)",$prev+1,$cnt-1);
                                    $_bpi3_ewt_total = sprintf("=SUM(AM%s:AM%s)",$prev+1,$cnt-1);
                                    $_bdo3_net_total = sprintf("=SUM(AN%s:AN%s)",$prev+1,$cnt-1);

                                    $_bpi6_amount_total =  sprintf("=SUM(AP%s:AP%s)",$prev+1,$cnt-1);
                                    $_bpi6_merch_total = sprintf("=SUM(AQ%s:AQ%s)",$prev+1,$cnt-1);
                                    $_bpi6_ewt_total = sprintf("=SUM(AR%s:AR%s)",$prev+1,$cnt-1);
                                    $_bpi6_net_total = sprintf("=SUM(AS%s:AS%s)",$prev+1,$cnt-1);

                                    $_bpi12_amount_total = sprintf("=SUM(AU%s:AU%s)",$prev+1,$cnt-1);
                                    $_bpi12_merch_total = sprintf("=SUM(AV%s:AV%s)",$prev+1,$cnt-1);
                                    $_bpi12_ewt_total = sprintf("=SUM(AW%s:AW%s)",$prev+1,$cnt-1);
                                    $_bpi12_net_total = sprintf("=SUM(AX%s:AX%s)",$prev+1,$cnt-1);


                                    $_citibank3_amount_total = sprintf("=SUM(AZ%s:AZ%s)",$prev+1,$cnt-1);
                                    $_citibank3_merch_total = sprintf("=SUM(BA%s:BA%s)",$prev+1,$cnt-1);
                                    $_citibank3_ewt_total = sprintf("=SUM(BB%s:BB%s)",$prev+1,$cnt-1);
                                    $_citibank3_net_total = sprintf("=SUM(BC%s:BC%s)",$prev+1,$cnt-1);

                                    $_citibank6_amount_total = sprintf("=SUM(BE%s:BE%s)",$prev+1,$cnt-1);
                                    $_citibank6_merch_total = sprintf("=SUM(BF%s:BF%s)",$prev+1,$cnt-1);
                                    $_citibank6_ewt_total = sprintf("=SUM(BG%s:BG%s)",$prev+1,$cnt-1);
                                    $_citibank6_net_total = sprintf("=SUM(BH%s:BH%s)",$prev+1,$cnt-1);

                                    $_citibank12_amount_total = sprintf("=SUM(BJ%s:BJ%s)",$prev+1,$cnt-1);
                                    $_citibank12_merch_total = sprintf("=SUM(BK%s:BK%s)",$prev+1,$cnt-1);
                                    $_citibank12_ewt_total = sprintf("=SUM(BL%s:BL%s)",$prev+1,$cnt-1);
                                    $_citibank12_net_total = sprintf("=SUM(BM%s:BM%s)",$prev+1,$cnt-1);

                                    $_hsbc3_amount_total = sprintf("=SUM(BO%s:BO%s)",$prev+1,$cnt-1);
                                    $_hsbc3_merch_total = sprintf("=SUM(BP%s:BP%s)",$prev+1,$cnt-1);
                                    $_hsbc3_ewt_total = sprintf("=SUM(BQ%s:BQ%s)",$prev+1,$cnt-1);
                                    $_hsbc3_net_total = sprintf("=SUM(BR%s:BR%s)",$prev+1,$cnt-1);

                                    $_hsbc6_amount_total = sprintf("=SUM(BT%s:BT%s)",$prev+1,$cnt-1);
                                    $_hsbc6_merch_total = sprintf("=SUM(BU%s:BU%s)",$prev+1,$cnt-1);
                                    $_hsbc6_ewt_total = sprintf("=SUM(BV%s:BV%s)",$prev+1,$cnt-1);
                                    $_hsbc6_net_total = sprintf("=SUM(BW%s:BW%s)",$prev+1,$cnt-1);

                                    $_hsbc12_amount_total = sprintf("=SUM(BY%s:BY%s)",$prev+1,$cnt-1);
                                    $_hsbc12_merch_total = sprintf("=SUM(BZ%s:BZ%s)",$prev+1,$cnt-1);
                                    $_hsbc12_ewt_total = sprintf("=SUM(CA%s:CA%s)",$prev+1,$cnt-1);
                                    $_hsbc12_net_total = sprintf("=SUM(CB%s:CB%s)",$prev+1,$cnt-1);


                                    $_east3_amount_total = sprintf("=SUM(CD%s:CD%s)",$prev+1,$cnt-1);
                                    $_east3_merch_total = sprintf("=SUM(CE%s:CE%s)",$prev+1,$cnt-1);
                                    $_east3_ewt_total = sprintf("=SUM(CF%s:CF%s)",$prev+1,$cnt-1);
                                    $_east3_net_total = sprintf("=SUM(CG%s:CG%s)",$prev+1,$cnt-1);

                                    $_east6_amount_total = sprintf("=SUM(CI%s:CI%s)",$prev+1,$cnt-1);
                                    $_east6_merch_total = sprintf("=SUM(CJ%s:CJ%s)",$prev+1,$cnt-1);
                                    $_east6_ewt_total = sprintf("=SUM(CK%s:CK%s)",$prev+1,$cnt-1);
                                    $_east6_net_total = sprintf("=SUM(CL%s:CL%s)",$prev+1,$cnt-1);

                                    $_east12_amount_total = sprintf("=SUM(CN%s:CN%s)",$prev+1,$cnt-1);
                                    $_east12_merch_total = sprintf("=SUM(CO%s:CO%s)",$prev+1,$cnt-1);
                                    $_east12_ewt_total = sprintf("=SUM(CP%s:CP%s)",$prev+1,$cnt-1);
                                    $_east12_net_total = sprintf("=SUM(CQ%s:CQ%s)",$prev+1,$cnt-1);

                                    $_metro3_amount_total = sprintf("=SUM(CS%s:CS%s)",$prev+1,$cnt-1);
                                    $_metro3_merch_total = sprintf("=SUM(CT%s:CT%s)",$prev+1,$cnt-1);
                                    $_metro3_ewt_total = sprintf("=SUM(CU%s:CU%s)",$prev+1,$cnt-1);
                                    $_metro3_net_total = sprintf("=SUM(CV%s:CV%s)",$prev+1,$cnt-1);

                                    $_metro6_amount_total = sprintf("=SUM(CX%s:CX%s)",$prev+1,$cnt-1);
                                    $_metro6_merch_total = sprintf("=SUM(CY%s:CY%s)",$prev+1,$cnt-1);
                                    $_metro6_ewt_total = sprintf("=SUM(CZ%s:CZ%s)",$prev+1,$cnt-1);
                                    $_metro6_net_total = sprintf("=SUM(DA%s:DA%s)",$prev+1,$cnt-1);

                                    $_metro12_amount_total = sprintf("=SUM(DC%s:DC%s)",$prev+1,$cnt-1);
                                    $_metro12_merch_total = sprintf("=SUM(DD%s:DD%s)",$prev+1,$cnt-1);
                                    $_metro12_ewt_total = sprintf("=SUM(DE%s:DE%s)",$prev+1,$cnt-1);
                                    $_metro12_net_total = sprintf("=SUM(DF%s:DF%s)",$prev+1,$cnt-1);


                                    $_diner3_amount_total = sprintf("=SUM(DH%s:DH%s)",$prev+1,$cnt-1);
                                    $_diner3_merch_total = sprintf("=SUM(DI%s:DI%s)",$prev+1,$cnt-1);
                                    $_diner3_ewt_total = sprintf("=SUM(DJ%s:DJ%s)",$prev+1,$cnt-1);
                                    $_diner3_net_total = sprintf("=SUM(DK%s:DK%s)",$prev+1,$cnt-1);

                                    $_diner6_amount_total = sprintf("=SUM(DM%s:DM%s)",$prev+1,$cnt-1);
                                    $_diner6_merch_total = sprintf("=SUM(DN%s:DN%s)",$prev+1,$cnt-1);
                                    $_diner6_ewt_total = sprintf("=SUM(DO%s:DO%s)",$prev+1,$cnt-1);
                                    $_diner6_net_total = sprintf("=SUM(DP%s:DP%s)",$prev+1,$cnt-1);

                                    $_diner12_amount_total = sprintf("=SUM(DR%s:DR%s)",$prev+1,$cnt-1);
                                    $_diner12_merch_total = sprintf("=SUM(DS%s:Ds%s)",$prev+1,$cnt-1);
                                    $_diner12_ewt_total = sprintf("=SUM(DT%s:DT%s)",$prev+1,$cnt-1);
                                    $_diner12_net_total = sprintf("=SUM(DU%s:DU%s)",$prev+1,$cnt-1);


                                    $_pnb3_amount_total = sprintf("=SUM(DW%s:DW%s)",$prev+1,$cnt-1);
                                    $_pnb3_merch_total = sprintf("=SUM(DX%s:DX%s)",$prev+1,$cnt-1);
                                    $_pnb3_ewt_total =sprintf("=SUM(DY%s:DY%s)",$prev+1,$cnt-1);
                                    $_pnb3_net_total =sprintf("=SUM(DZ%s:DZ%s)",$prev+1,$cnt-1);

                                    $_pnb6_amount_total = sprintf("=SUM(EB%s:EB%s)",$prev+1,$cnt-1);
                                    $_pnb6_merch_total = sprintf("=SUM(EC%s:EC%s)",$prev+1,$cnt-1);
                                    $_pnb6_ewt_total = sprintf("=SUM(ED%s:ED%s)",$prev+1,$cnt-1);
                                    $_pnb6_net_total = sprintf("=SUM(EE%s:EE%s)",$prev+1,$cnt-1);

                                    $_pnb12_amount_total = sprintf("=SUM(EG%s:EG%s)",$prev+1,$cnt-1);
                                    $_pnb12_merch_total = sprintf("=SUM(EH%s:EH%s)",$prev+1,$cnt-1);
                                    $_pnb12_ewt_total = sprintf("=SUM(EI%s:EI%s)",$prev+1,$cnt-1);
                                    $_pnb12_net_total = sprintf("=SUM(EJ%s:EJ%s)",$prev+1,$cnt-1);


                                    $_rcbc3_amount_total = sprintf("=SUM(EL%s:EL%s)",$prev+1,$cnt-1);
                                    $_rcbc3_merch_total = sprintf("=SUM(EM%s:EM%s)",$prev+1,$cnt-1);
                                    $_rcbc3_ewt_total = sprintf("=SUM(EN%s:EN%s)",$prev+1,$cnt-1);
                                    $_rcbc3_net_total = sprintf("=SUM(EO%s:EO%s)",$prev+1,$cnt-1);

                                    $_rcbc6_amount_total = sprintf("=SUM(EQ%s:EQ%s)",$prev+1,$cnt-1);
                                    $_rcbc6_merch_total = sprintf("=SUM(ER%s:ER%s)",$prev+1,$cnt-1);
                                    $_rcbc6_ewt_total = sprintf("=SUM(ES%s:ES%s)",$prev+1,$cnt-1);
                                    $_rcbc6_net_total = sprintf("=SUM(ET%s:ET%s)",$prev+1,$cnt-1);

                                    $_rcbc12_amount_total = sprintf("=SUM(EV%s:EV%s)",$prev+1,$cnt-1);
                                    $_rcbc12_merch_total = sprintf("=SUM(EW%s:EW%s)",$prev+1,$cnt-1);
                                    $_rcbc12_ewt_total = sprintf("=SUM(EX%s:EX%s)",$prev+1,$cnt-1);
                                    $_rcbc12_net_total = sprintf("=SUM(EY%s:EY%s)",$prev+1,$cnt-1);

                                    $_union3_amount_total = sprintf("=SUM(FA%s:FA%s)",$prev+1,$cnt-1);
                                    $_union3_merch_total = sprintf("=SUM(FB%s:FB%s)",$prev+1,$cnt-1);
                                    $_union3_ewt_total = sprintf("=SUM(FC%s:FC%s)",$prev+1,$cnt-1);
                                    $_union3_net_total = sprintf("=SUM(FD%s:FD%s)",$prev+1,$cnt-1);

                                    

                                    $_union6_amount_total = sprintf("=SUM(FF%s:FF%s)",$prev+1,$cnt-1);
                                    $_union6_merch_total = sprintf("=SUM(FG%s:FG%s)",$prev+1,$cnt-1);
                                    $_union6_ewt_total = sprintf("=SUM(FH%s:FH%s)",$prev+1,$cnt-1);
                                    $_union6_net_total = sprintf("=SUM(FI%s:FI%s)",$prev+1,$cnt-1);

                                    $_union12_amount_total = sprintf("=SUM(FK%s:FK%s)",$prev+1,$cnt-1);
                                    $_union12_merch_total = sprintf("=SUM(FL%s:FL%s)",$prev+1,$cnt-1);
                                    $_union12_ewt_total = sprintf("=SUM(FM%s:FM%s)",$prev+1,$cnt-1);
                                    $_union12_net_total = sprintf("=SUM(FN%s:FN%s)",$prev+1,$cnt-1);


                                    }






                                    $sheet->row($cnt, function($row) {

 
                                        $row->setBackground('##f1c40f');

                                    });

                                     $sheet->row($cnt,array(
                                        "DAILY SUB TOTAL",
                                         '' ,
                                        ' ',
                                        ' ',
                                        $_atm_total,
                                        $_credit_card_total,
                                        ' ',
                                        $_atm_amount_total,
                                        $_atm_merch_total,
                                        $_atm_net_total,
                                        ' ',
                                        $_str_amount_total,
                                        $_str_merch_total,
                                        $_str_ewt_total,
                                        $_str_net_total,
                                        ' ',
                                        $_jcb_amount_total,
                                        $_jcb_merch_total,
                                        $_jcb_ewt_total,
                                        $_jcb_net_total,
                                        ' ',
                                        $_bdo3_amount_total,
                                        $_bdo3_merch_total,
                                        $_bdo3_ewt_total,
                                        $_bdo3_net_total,
                                        ' ',
                                        $_bdo6_amount_total,
                                        $_bdo6_merch_total,
                                        $_bdo6_ewt_total,
                                        $_bdo6_net_total,
                                        ' ',
                                        $_bdo12_amount_total,
                                        $_bdo12_merch_total,
                                        $_bdo12_ewt_total,
                                        $_bdo12_net_total,
                                        ' ',
                                        $_bpi3_amount_total,
                                        $_bpi3_merch_total,
                                        $_bpi3_ewt_total,
                                        $_bdo3_net_total,
                                        ' ',
                                        $_bpi6_amount_total,
                                        $_bpi6_merch_total,
                                        $_bpi6_ewt_total,
                                        $_bpi6_net_total,
                                        ' ',
                                        $_bpi12_amount_total,
                                        $_bpi12_merch_total,
                                        $_bpi12_ewt_total,
                                        $_bpi12_net_total,
                                        ' ',
                                        $_citibank3_amount_total,
                                        $_citibank3_merch_total,
                                        $_citibank3_ewt_total,
                                        $_citibank3_net_total,
                                        ' ',
                                        $_citibank6_amount_total,
                                        $_citibank6_merch_total ,
                                        $_citibank6_ewt_total ,
                                        $_citibank6_net_total ,
                                        ' ',
                                        $_citibank12_amount_total ,
                                        $_citibank12_merch_total ,
                                        $_citibank12_ewt_total ,
                                        $_citibank12_net_total ,
                                        ' ',
                                        $_hsbc3_amount_total ,
                                        $_hsbc3_merch_total ,
                                        $_hsbc3_ewt_total,
                                        $_hsbc3_net_total ,
                                        ' ',
                                        $_hsbc6_amount_total ,
                                        $_hsbc6_merch_total,
                                        $_hsbc6_ewt_total ,
                                        $_hsbc6_net_total,
                                        ' ',
                                        $_hsbc12_amount_total,
                                        $_hsbc12_merch_total,
                                        $_hsbc12_ewt_total ,
                                        $_hsbc12_net_total ,
                                        ' ',
                                        $_east3_amount_total,
                                        $_east3_merch_total ,
                                        $_east3_ewt_total ,
                                        $_east3_net_total ,
                                        ' ',
                                        $_east6_amount_total ,
                                        $_east6_merch_total ,
                                        $_east6_ewt_total ,
                                        $_east6_net_total ,
                                        ' ',
                                        $_east12_amount_total ,
                                        $_east12_merch_total ,
                                        $_east12_ewt_total ,
                                        $_east12_net_total ,
                                        ' ',
                                        $_metro3_amount_total ,
                                        $_metro3_merch_total ,
                                        $_metro3_ewt_total ,
                                        $_metro3_net_total ,
                                        ' ',
                                        $_metro6_amount_total ,
                                        $_metro6_merch_total ,
                                        $_metro6_ewt_total ,
                                        $_metro6_net_total ,
                                        ' ',
                                        $_metro12_amount_total ,
                                        $_metro12_merch_total ,
                                        $_metro12_ewt_total ,
                                        $_metro12_net_total ,
                                        ' ',
                                        
                                        $_diner3_amount_total ,
                                        $_diner3_merch_total ,
                                        $_diner3_ewt_total ,
                                        $_diner3_net_total ,
                                        ' ',
                                        $_diner6_amount_total ,
                                        $_diner6_merch_total ,
                                        $_diner6_ewt_total ,
                                        $_diner6_net_total ,
                                        ' ',
                                        $_diner12_amount_total ,
                                        $_diner12_merch_total ,
                                        $_diner12_ewt_total ,
                                        $_diner12_net_total ,
                                        ' ',
                                        $_pnb3_amount_total ,
                                        $_pnb3_merch_total ,
                                        $_pnb3_ewt_total ,
                                        $_pnb3_net_total ,
                                        ' ',
                                        $_pnb6_amount_total ,
                                        $_pnb6_merch_total,
                                        $_pnb6_ewt_total ,
                                        $_pnb6_net_total ,
                                        ' ',
                                        $_pnb12_amount_total ,
                                        $_pnb12_merch_total ,
                                        $_pnb12_ewt_total ,
                                        $_pnb12_net_total ,
                                        ' ',
                                        $_rcbc3_amount_total,
                                        $_rcbc3_merch_total ,
                                        $_rcbc3_ewt_total ,
                                        $_rcbc3_net_total ,
                                        ' ',
                                        $_rcbc6_amount_total ,
                                       $_rcbc6_merch_total ,
                                        $_rcbc6_ewt_total ,
                                        $_rcbc6_net_total ,
                                        ' ',
                                        $_rcbc12_amount_total ,
                                        $_rcbc12_merch_total ,
                                        $_rcbc12_ewt_total ,
                                        $_rcbc12_net_total ,
                                        ' ',
                                        $_union3_amount_total ,
                                        $_union3_merch_total,
                                        $_union3_ewt_total ,
                                        $_union3_net_total ,
                                        
                                        ' ',
                                        $_union6_amount_total ,
                                        $_union6_merch_total ,
                                        $_union6_ewt_total ,
                                        $_union6_net_total ,
                                        ' ',
                                        $_union12_amount_total ,
                                        $_union12_merch_total ,
                                        $_union12_ewt_total,
                                        $_union12_net_total ));

                                           
                                        

                                      

                                     $prev = $cnt;
                                       $cnt++;

                                }








                                     $sheet->row($cnt,array( date('d-M',strtotime($data->local_time)),
                                         $data->invoice_no,
                                        $data->ref_no,
                                        $data->customer_name,
                                        $data->atm_amount,
                                        $data->credit_card_amount,
                                        ' ',
                                        $data->atm_amount,
                                        $data->atm_amount * 0.02,
                                        $data->atm_amount -  ($data->atm_amount * 0.02),
                                        ' ',
                                        $data->other_jcb,
                                        $data->other_jcb * 0.0275,
                                        $data->other_jcb * 0.005,
                                        $data->other_jcb - ($data->other_jcb * 0.0275) - ($data->other_jcb * 0.005),
                                        ' ',
                                        $data->jcb_amount,
                                        $data->jcb_amount * 0.03,
                                        $data->jcb_amount * 0.005,
                                        $data->jcb_amount - ($data->jcb_amount * 0.03) - ($data->jcb_amount * 0.005),
                                        ' ',
                                        $bdo_amount_3,
                                        $bdo_subsidy_3,
                                        $bdo_amount_3 * 0.005,
                                        $bdo_amount_3 - $bdo_subsidy_3 - ($bdo_amount_3 * 0.005),
                                        ' ',
                                        $bdo_amount_6,
                                        $bdo_subsidy_6,
                                        $bdo_amount_6 * 0.005,
                                        $bdo_amount_6 - $bdo_subsidy_6 - ($bdo_amount_6 * 0.005),
                                        ' ',
                                        $bdo_amount_12,
                                        $bdo_subsidy_12,
                                        $bdo_amount_12 * 0.005,
                                        $bdo_amount_12 - $bdo_subsidy_12 - ($bdo_amount_12 * 0.005),
                                        ' ',
                                        $bpi_amount_3,
                                        $bpi_subsidy_3,
                                        $bpi_amount_3 * 0.005,
                                        $bpi_amount_3 - $bpi_subsidy_3 - ($bpi_amount_3 * 0.005),
                                        ' ',
                                        $bpi_amount_6,
                                        $bpi_subsidy_6,
                                        $bpi_amount_6 * 0.005,
                                        $bpi_amount_6 - $bpi_subsidy_6 - ($bpi_amount_6 * 0.005),
                                        ' ',
                                        $bpi_amount_12,
                                        $bpi_subsidy_12,
                                        $bpi_amount_12 * 0.005,
                                        $bpi_amount_12 - $bpi_subsidy_12 - ($bpi_amount_12 * 0.005),
                                        ' ',
                                        $citibank_amount_3,
                                        $citibank_subsidy_3,
                                        $citibank_amount_3 * 0.005,
                                        $citibank_amount_3 - $citibank_subsidy_3 - ($citibank_amount_3 * 0.005),
                                        ' ',
                                        $citibank_amount_6,
                                        $citibank_subsidy_6,
                                        $citibank_amount_6 * 0.005,
                                         $citibank_amount_6 - $citibank_subsidy_6 - ($citibank_amount_6 * 0.005),
                                        ' ',
                                        $citibank_amount_12,
                                        $citibank_subsidy_12,
                                        $citibank_amount_12 * 0.005,
                                        $citibank_amount_12 - $citibank_subsidy_12 - ($citibank_amount_12 * 0.005),
                                        ' ',
                                        $hsbc_amount_3,
                                        $hsbc_subsidy_3,
                                        $hsbc_amount_3 * 0.005,
                                        $hsbc_amount_3 - $hsbc_subsidy_3 - ($hsbc_amount_3 * 0.005),
                                        ' ',
                                        $hsbc_amount_6,
                                        $hsbc_subsidy_6,
                                        $hsbc_amount_6 * 0.005,
                                        $hsbc_amount_6 - $hsbc_subsidy_6 - ($hsbc_amount_6 * 0.005),
                                        ' ',
                                        $hsbc_amount_12,
                                        $hsbc_subsidy_12,
                                        $hsbc_amount_12 * 0.005,
                                        $hsbc_amount_12 - $hsbc_subsidy_12 - ($hsbc_amount_12 * 0.005),
                                        ' ',
                                        $eastwest_amount_3,
                                        $eastwest_subsidy_3,
                                        $eastwest_amount_3 * 0.005,
                                        $eastwest_amount_3 - $eastwest_subsidy_3 - ($eastwest_amount_3 * 0.005),
                                        ' ',
                                        $eastwest_amount_6,
                                        $eastwest_subsidy_6,
                                        $eastwest_amount_6 * 0.005,
                                        $eastwest_amount_6 - $eastwest_subsidy_6 - ($eastwest_amount_6 * 0.005),
                                        ' ',
                                        $eastwest_amount_12,
                                        $eastwest_subsidy_12,
                                        $eastwest_amount_12 * 0.005,
                                        $eastwest_amount_12 - $eastwest_subsidy_12 - ($eastwest_amount_12 * 0.005),
                                        ' ',
                                        $metrobank_amount_3,
                                        $metrobank_subsidy_3,
                                        $metrobank_amount_3 * 0.005,
                                        $metrobank_amount_3 - $metrobank_subsidy_3 - ($metrobank_amount_3 * 0.005),
                                        ' ',
                                        $metrobank_amount_6,
                                        $metrobank_subsidy_6,
                                        $metrobank_amount_6 * 0.005,
                                         $metrobank_amount_6 - $metrobank_subsidy_6 - ($metrobank_amount_6 * 0.005),
                                        ' ',
                                        $metrobank_amount_12,
                                         $metrobank_subsidy_12,
                                        $metrobank_amount_12 * 0.005,
                                        $metrobank_amount_12 - $metrobank_subsidy_12 - ($metrobank_amount_12 * 0.005),
                                        ' ',
                                        
                                        $dinner_amount_3,
                                        $dinner_subsidy_3,
                                        $dinner_amount_3 * 0.005,
                                        $dinner_amount_3 - $dinner_subsidy_3 - ($dinner_amount_3 * 0.005),
                                        ' ',
                                        $dinner_amount_6,
                                        $dinner_subsidy_6,
                                        $dinner_amount_6 * 0.005,
                                        $dinner_amount_6 - $dinner_subsidy_6 - ($dinner_amount_6 * 0.005),
                                        ' ',
                                        $dinner_amount_12,
                                        $dinner_subsidy_12,
                                        $dinner_amount_12 * 0.005,
                                        $dinner_amount_12 - $dinner_subsidy_12 - ($dinner_amount_12 * 0.005),
                                        ' ',
                                        $pnb_amount_3,
                                        $pnb_subsidy_3,
                                        $pnb_amount_3 * 0.005,
                                        $pnb_amount_3 - $pnb_subsidy_3 - ($pnb_amount_3 * 0.005),
                                        ' ',
                                        $pnb_amount_6,
                                        $pnb_subsidy_6,
                                        $pnb_amount_6 * 0.005,
                                        $pnb_amount_6 - $pnb_subsidy_6 - ($pnb_amount_6 * 0.005),
                                        ' ',
                                        $pnb_amount_12,
                                        $pnb_subsidy_12,
                                        $pnb_amount_12 * 0.005,
                                        $pnb_amount_12 - $pnb_subsidy_12 - ($pnb_amount_12 * 0.005),
                                        ' ',
                                        $rcbc_amount_3,
                                        $rcbc_subsidy_3,
                                        $rcbc_amount_3 * 0.005,
                                        $rcbc_amount_3 - $rcbc_subsidy_3 - ($rcbc_amount_3 * 0.005),
                                        ' ',
                                        $rcbc_amount_6,
                                        $rcbc_subsidy_6,
                                         $rcbc_amount_6 * 0.005,
                                        $rcbc_amount_6 - $rcbc_subsidy_6 - ($rcbc_amount_6 * 0.005),
                                        ' ',
                                        $rcbc_amount_12,
                                        $rcbc_subsidy_12,
                                        $rcbc_amount_12 * 0.005,
                                        $rcbc_amount_12 - $rcbc_subsidy_12 - ($rcbc_amount_12 * 0.005),
                                        ' ',
                                        $unionbank_amount_3,
                                        $unionbank_subsidy_3,
                                        $unionbank_amount_3 * 0.005,
                                        $unionbank_amount_3 - $unionbank_subsidy_3 - ($unionbank_amount_3 * 0.005),
                                        
                                        ' ',
                                        $unionbank_amount_6,
                                        $unionbank_subsidy_6,
                                        $unionbank_amount_6 * 0.005,
                                        $unionbank_amount_6 - $unionbank_subsidy_6 - ($unionbank_amount_6 * 0.005),
                                        ' ',
                                        $unionbank_amount_12,
                                        $unionbank_subsidy_12,
                                        $unionbank_amount_12 * 0.005,
                                        $unionbank_amount_12 - $unionbank_subsidy_12 - ($unionbank_amount_12 * 0.005)));



                                 $cnt++;
                                   $index++;
                                 $last_date =  $first_date;
                                }

                                 $_atm_total = 0.00;
                                    $_credit_card_total = 0.00;

                                    $_atm_amount_total = 0.00;
                                    $_atm_merch_total = 0.00;
                                    $_atm_net_total =0.00;

                                    $_str_amount_total = 0.00;
                                    $_str_merch_total = 0.00;
                                    $_str_ewt_total =0.00;
                                    $_str_net_total =0.00;

                                    $_jcb_amount_total = 0.00;
                                    $_jcb_merch_total = 0.00;
                                    $_jcb_ewt_total =0.00;
                                    $_jcb_net_total =0.00;

                                    $_bdo3_amount_total = 0.00;
                                    $_bdo3_merch_total = 0.00;
                                    $_bdo3_ewt_total =0.00;
                                    $_bdo3_net_total =0.00;

                                    $_bdo6_amount_total = 0.00;
                                    $_bdo6_merch_total = 0.00;
                                    $_bdo6_ewt_total =0.00;
                                    $_bdo6_net_total =0.00;

                                    $_bdo12_amount_total = 0.00;
                                    $_bdo12_merch_total = 0.00;
                                    $_bdo12_ewt_total =0.00;
                                    $_bdo12_net_total =0.00;

                                    $_bpi3_amount_total = 0.00;
                                    $_bpi3_merch_total = 0.00;
                                    $_bpi3_ewt_total =0.00;
                                    $_bdo3_net_total =0.00;

                                    $_bpi6_amount_total = 0.00;
                                    $_bpi6_merch_total = 0.00;
                                    $_bpi6_ewt_total =0.00;
                                    $_bpi6_net_total =0.00;

                                    $_bpi12_amount_total = 0.00;
                                    $_bpi12_merch_total = 0.00;
                                    $_bpi12_ewt_total =0.00;
                                    $_bpi12_net_total =0.00;


                                    $_citibank3_amount_total = 0.00;
                                    $_citibank3_merch_total = 0.00;
                                    $_citibank3_ewt_total =0.00;
                                    $_citibank3_net_total =0.00;

                                    $_citibank6_amount_total = 0.00;
                                    $_citibank6_merch_total = 0.00;
                                    $_citibank6_ewt_total =0.00;
                                    $_citibank6_net_total =0.00;

                                    $_citibank12_amount_total = 0.00;
                                    $_citibank12_merch_total = 0.00;
                                    $_citibank12_ewt_total =0.00;
                                    $_citibank12_net_total =0.00;

                                    $_hsbc3_amount_total = 0.00;
                                    $_hsbc3_merch_total = 0.00;
                                    $_hsbc3_ewt_total =0.00;
                                    $_hsbc3_net_total = 0.00;

                                    $_hsbc6_amount_total = 0.00;
                                    $_hsbc6_merch_total = 0.00;
                                    $_hsbc6_ewt_total = 0.00;
                                    $_hsbc6_net_total = 0.00;

                                    $_hsbc12_amount_total = 0.00;
                                    $_hsbc12_merch_total = 0.00;
                                    $_hsbc12_ewt_total = 0.00;
                                    $_hsbc12_net_total = 0.00;


                                    $_east3_amount_total = 0.00;
                                    $_east3_merch_total = 0.00;
                                    $_east3_ewt_total =0.00;
                                    $_east3_net_total =0.00;

                                    $_east6_amount_total = 0.00;
                                    $_east6_merch_total = 0.00;
                                    $_east6_ewt_total =0.00;
                                    $_east6_net_total =0.00;

                                    $_east12_amount_total = 0.00;
                                    $_east12_merch_total = 0.00;
                                    $_east12_ewt_total =0.00;
                                    $_east12_net_total =0.00;

                                    $_metro3_amount_total = 0.00;
                                    $_metro3_merch_total = 0.00;
                                    $_metro3_ewt_total =0.00;
                                    $_metro3_net_total =0.00;

                                    $_metro6_amount_total = 0.00;
                                    $_metro6_merch_total = 0.00;
                                    $_metro6_ewt_total =0.00;
                                    $_metro6_net_total =0.00;

                                    $_metro12_amount_total = 0.00;
                                    $_metro12_merch_total = 0.00;
                                    $_metro12_ewt_total =0.00;
                                    $_metro12_net_total =0.00;


                                    $_diner3_amount_total = 0.00;
                                    $_diner3_merch_total = 0.00;
                                    $_diner3_ewt_total =0.00;
                                    $_diner3_net_total =0.00;

                                    $_diner6_amount_total = 0.00;
                                    $_diner6_merch_total = 0.00;
                                    $_diner6_ewt_total =0.00;
                                    $_diner6_net_total =0.00;

                                    $_diner12_amount_total = 0.00;
                                    $_diner12_merch_total = 0.00;
                                    $_diner12_ewt_total =0.00;
                                    $_diner12_net_total =0.00;


                                    $_pnb3_amount_total = 0.00;
                                    $_pnb3_merch_total = 0.00;
                                    $_pnb3_ewt_total =0.00;
                                    $_pnb3_net_total =0.00;

                                    $_pnb6_amount_total = 0.00;
                                    $_pnb6_merch_total = 0.00;
                                    $_pnb6_ewt_total =0.00;
                                    $_pnb6_net_total =0.00;

                                    $_pnb12_amount_total = 0.00;
                                    $_pnb12_merch_total = 0.00;
                                    $_pnb12_ewt_total =0.00;
                                    $_pnb12_net_total =0.00;


                                    $_rcbc3_amount_total = 0.00;
                                    $_rcbc3_merch_total = 0.00;
                                    $_rcbc3_ewt_total =0.00;
                                    $_rcbc3_net_total =0.00;

                                    $_rcbc6_amount_total = 0.00;
                                    $_rcbc6_merch_total = 0.00;
                                    $_rcbc6_ewt_total =0.00;
                                    $_rcbc6_net_total =0.00;

                                    $_rcbc12_amount_total = 0.00;
                                    $_rcbc12_merch_total = 0.00;
                                    $_rcbc12_ewt_total =0.00;
                                    $_rcbc12_net_total =0.00;

                                    $_union3_amount_total = 0.00;
                                    $_union3_merch_total = 0.00;
                                    $_union3_ewt_total =0.00;
                                    $_union3_net_total =0.00;

                                    $_union6_amount_total = 0.00;
                                    $_union6_merch_total = 0.00;
                                    $_union6_ewt_total =0.00;
                                    $_union6_net_total =0.00;

                                    $_union12_amount_total = 0.00;
                                    $_union12_merch_total = 0.00;
                                    $_union12_ewt_total =0.00;
                                    $_union12_net_total =0.00;


                            	       $_atm_total = sprintf("=SUM(E%s:E%s)",$prev+1,$cnt-1);
                                    $_credit_card_total = sprintf("=SUM(F%s:F%s)",$prev+1,$cnt-1);

                                    $_atm_amount_total = sprintf("=SUM(H%s:H%s)",$prev+1,$cnt-1);
                                    $_atm_merch_total = sprintf("=SUM(I%s:I%s)",$prev+1,$cnt-1);
                                    $_atm_net_total = sprintf("=SUM(J%s:J%s)",$prev+1,$cnt-1);

                                    $_str_amount_total = sprintf("=SUM(L%s:L%s)",$prev+1,$cnt-1);
                                    $_str_merch_total = sprintf("=SUM(M%s:M%s)",$prev+1,$cnt-1);
                                    $_str_ewt_total = sprintf("=SUM(N%s:N%s)",$prev+1,$cnt-1);
                                    $_str_net_total =sprintf("=SUM(O%s:O%s)",$prev+1,$cnt-1);

                                    $_jcb_amount_total = sprintf("=SUM(Q%s:Q%s)",$prev+1,$cnt-1);
                                    $_jcb_merch_total = sprintf("=SUM(R%s:R%s)",$prev+1,$cnt-1);
                                    $_jcb_ewt_total = sprintf("=SUM(S%s:S%s)",$prev+1,$cnt-1);
                                    $_jcb_net_total =sprintf("=SUM(T%s:T%s)",$prev+1,$cnt-1);

                                    $_bdo3_amount_total = sprintf("=SUM(V%s:V%s)",$prev+1,$cnt-1);
                                    $_bdo3_merch_total = sprintf("=SUM(W%s:W%s)",$prev+1,$cnt-1);
                                    $_bdo3_ewt_total = sprintf("=SUM(X%s:X%s)",$prev+1,$cnt-1);
                                    $_bdo3_net_total = sprintf("=SUM(Y%s:Y%s)",$prev+1,$cnt-1);

                                    $_bdo6_amount_total = sprintf("=SUM(AA%s:AA%s)",$prev+1,$cnt-1);
                                    $_bdo6_merch_total = sprintf("=SUM(AB%s:AB%s)",$prev+1,$cnt-1);
                                    $_bdo6_ewt_total = sprintf("=SUM(AC%s:AC%s)",$prev+1,$cnt-1);
                                    $_bdo6_net_total = sprintf("=SUM(AD%s:AD%s)",$prev+1,$cnt-1);

                                    $_bdo12_amount_total = sprintf("=SUM(AF%s:AF%s)",$prev+1,$cnt-1);
                                    $_bdo12_merch_total = sprintf("=SUM(AG%s:AG%s)",$prev+1,$cnt-1);
                                    $_bdo12_ewt_total = sprintf("=SUM(AH%s:AH%s)",$prev+1,$cnt-1);
                                    $_bdo12_net_total = sprintf("=SUM(AI%s:AI%s)",$prev+1,$cnt-1);

                                    $_bpi3_amount_total = sprintf("=SUM(AK%s:AK%s)",$prev+1,$cnt-1);
                                    $_bpi3_merch_total = sprintf("=SUM(AL%s:AL%s)",$prev+1,$cnt-1);
                                    $_bpi3_ewt_total = sprintf("=SUM(AM%s:AM%s)",$prev+1,$cnt-1);
                                    $_bdo3_net_total = sprintf("=SUM(AN%s:AN%s)",$prev+1,$cnt-1);

                                    $_bpi6_amount_total =  sprintf("=SUM(AP%s:AP%s)",$prev+1,$cnt-1);
                                    $_bpi6_merch_total = sprintf("=SUM(AQ%s:AQ%s)",$prev+1,$cnt-1);
                                    $_bpi6_ewt_total = sprintf("=SUM(AR%s:AR%s)",$prev+1,$cnt-1);
                                    $_bpi6_net_total = sprintf("=SUM(AS%s:AS%s)",$prev+1,$cnt-1);

                                    $_bpi12_amount_total = sprintf("=SUM(AU%s:AU%s)",$prev+1,$cnt-1);
                                    $_bpi12_merch_total = sprintf("=SUM(AV%s:AV%s)",$prev+1,$cnt-1);
                                    $_bpi12_ewt_total = sprintf("=SUM(AW%s:AW%s)",$prev+1,$cnt-1);
                                    $_bpi12_net_total = sprintf("=SUM(AX%s:AX%s)",$prev+1,$cnt-1);


                                    $_citibank3_amount_total = sprintf("=SUM(AZ%s:AZ%s)",$prev+1,$cnt-1);
                                    $_citibank3_merch_total = sprintf("=SUM(BA%s:BA%s)",$prev+1,$cnt-1);
                                    $_citibank3_ewt_total = sprintf("=SUM(BB%s:BB%s)",$prev+1,$cnt-1);
                                    $_citibank3_net_total = sprintf("=SUM(BC%s:BC%s)",$prev+1,$cnt-1);

                                    $_citibank6_amount_total = sprintf("=SUM(BE%s:BE%s)",$prev+1,$cnt-1);
                                    $_citibank6_merch_total = sprintf("=SUM(BF%s:BF%s)",$prev+1,$cnt-1);
                                    $_citibank6_ewt_total = sprintf("=SUM(BG%s:BG%s)",$prev+1,$cnt-1);
                                    $_citibank6_net_total = sprintf("=SUM(BH%s:BH%s)",$prev+1,$cnt-1);

                                    $_citibank12_amount_total = sprintf("=SUM(BJ%s:BJ%s)",$prev+1,$cnt-1);
                                    $_citibank12_merch_total = sprintf("=SUM(BK%s:BK%s)",$prev+1,$cnt-1);
                                    $_citibank12_ewt_total = sprintf("=SUM(BL%s:BL%s)",$prev+1,$cnt-1);
                                    $_citibank12_net_total = sprintf("=SUM(BM%s:BM%s)",$prev+1,$cnt-1);

                                    $_hsbc3_amount_total = sprintf("=SUM(BO%s:BO%s)",$prev+1,$cnt-1);
                                    $_hsbc3_merch_total = sprintf("=SUM(BP%s:BP%s)",$prev+1,$cnt-1);
                                    $_hsbc3_ewt_total = sprintf("=SUM(BQ%s:BQ%s)",$prev+1,$cnt-1);
                                    $_hsbc3_net_total = sprintf("=SUM(BR%s:BR%s)",$prev+1,$cnt-1);

                                    $_hsbc6_amount_total = sprintf("=SUM(BT%s:BT%s)",$prev+1,$cnt-1);
                                    $_hsbc6_merch_total = sprintf("=SUM(BU%s:BU%s)",$prev+1,$cnt-1);
                                    $_hsbc6_ewt_total = sprintf("=SUM(BV%s:BV%s)",$prev+1,$cnt-1);
                                    $_hsbc6_net_total = sprintf("=SUM(BW%s:BW%s)",$prev+1,$cnt-1);

                                    $_hsbc12_amount_total = sprintf("=SUM(BY%s:BY%s)",$prev+1,$cnt-1);
                                    $_hsbc12_merch_total = sprintf("=SUM(BZ%s:BZ%s)",$prev+1,$cnt-1);
                                    $_hsbc12_ewt_total = sprintf("=SUM(CA%s:CA%s)",$prev+1,$cnt-1);
                                    $_hsbc12_net_total = sprintf("=SUM(CB%s:CB%s)",$prev+1,$cnt-1);


                                    $_east3_amount_total = sprintf("=SUM(CD%s:CD%s)",$prev+1,$cnt-1);
                                    $_east3_merch_total = sprintf("=SUM(CE%s:CE%s)",$prev+1,$cnt-1);
                                    $_east3_ewt_total = sprintf("=SUM(CF%s:CF%s)",$prev+1,$cnt-1);
                                    $_east3_net_total = sprintf("=SUM(CG%s:CG%s)",$prev+1,$cnt-1);

                                    $_east6_amount_total = sprintf("=SUM(CI%s:CI%s)",$prev+1,$cnt-1);
                                    $_east6_merch_total = sprintf("=SUM(CJ%s:CJ%s)",$prev+1,$cnt-1);
                                    $_east6_ewt_total = sprintf("=SUM(CK%s:CK%s)",$prev+1,$cnt-1);
                                    $_east6_net_total = sprintf("=SUM(CL%s:CL%s)",$prev+1,$cnt-1);

                                    $_east12_amount_total = sprintf("=SUM(CN%s:CN%s)",$prev+1,$cnt-1);
                                    $_east12_merch_total = sprintf("=SUM(CO%s:CO%s)",$prev+1,$cnt-1);
                                    $_east12_ewt_total = sprintf("=SUM(CP%s:CP%s)",$prev+1,$cnt-1);
                                    $_east12_net_total = sprintf("=SUM(CQ%s:CQ%s)",$prev+1,$cnt-1);

                                    $_metro3_amount_total = sprintf("=SUM(CS%s:CS%s)",$prev+1,$cnt-1);
                                    $_metro3_merch_total = sprintf("=SUM(CT%s:CT%s)",$prev+1,$cnt-1);
                                    $_metro3_ewt_total = sprintf("=SUM(CU%s:CU%s)",$prev+1,$cnt-1);
                                    $_metro3_net_total = sprintf("=SUM(CV%s:CV%s)",$prev+1,$cnt-1);

                                    $_metro6_amount_total = sprintf("=SUM(CX%s:CX%s)",$prev+1,$cnt-1);
                                    $_metro6_merch_total = sprintf("=SUM(CY%s:CY%s)",$prev+1,$cnt-1);
                                    $_metro6_ewt_total = sprintf("=SUM(CZ%s:CZ%s)",$prev+1,$cnt-1);
                                    $_metro6_net_total = sprintf("=SUM(DA%s:DA%s)",$prev+1,$cnt-1);

                                    $_metro12_amount_total = sprintf("=SUM(DC%s:DC%s)",$prev+1,$cnt-1);
                                    $_metro12_merch_total = sprintf("=SUM(DD%s:DD%s)",$prev+1,$cnt-1);
                                    $_metro12_ewt_total = sprintf("=SUM(DE%s:DE%s)",$prev+1,$cnt-1);
                                    $_metro12_net_total = sprintf("=SUM(DF%s:DF%s)",$prev+1,$cnt-1);


                                    $_diner3_amount_total = sprintf("=SUM(DH%s:DH%s)",$prev+1,$cnt-1);
                                    $_diner3_merch_total = sprintf("=SUM(DI%s:DI%s)",$prev+1,$cnt-1);
                                    $_diner3_ewt_total = sprintf("=SUM(DJ%s:DJ%s)",$prev+1,$cnt-1);
                                    $_diner3_net_total = sprintf("=SUM(DK%s:DK%s)",$prev+1,$cnt-1);

                                    $_diner6_amount_total = sprintf("=SUM(DM%s:DM%s)",$prev+1,$cnt-1);
                                    $_diner6_merch_total = sprintf("=SUM(DN%s:DN%s)",$prev+1,$cnt-1);
                                    $_diner6_ewt_total = sprintf("=SUM(DO%s:DO%s)",$prev+1,$cnt-1);
                                    $_diner6_net_total = sprintf("=SUM(DP%s:DP%s)",$prev+1,$cnt-1);

                                    $_diner12_amount_total = sprintf("=SUM(DR%s:DR%s)",$prev+1,$cnt-1);
                                    $_diner12_merch_total = sprintf("=SUM(DS%s:Ds%s)",$prev+1,$cnt-1);
                                    $_diner12_ewt_total = sprintf("=SUM(DT%s:DT%s)",$prev+1,$cnt-1);
                                    $_diner12_net_total = sprintf("=SUM(DU%s:DU%s)",$prev+1,$cnt-1);


                                    $_pnb3_amount_total = sprintf("=SUM(DW%s:DW%s)",$prev+1,$cnt-1);
                                    $_pnb3_merch_total = sprintf("=SUM(DX%s:DX%s)",$prev+1,$cnt-1);
                                    $_pnb3_ewt_total =sprintf("=SUM(DY%s:DY%s)",$prev+1,$cnt-1);
                                    $_pnb3_net_total =sprintf("=SUM(DZ%s:DZ%s)",$prev+1,$cnt-1);

                                    $_pnb6_amount_total = sprintf("=SUM(EB%s:EB%s)",$prev+1,$cnt-1);
                                    $_pnb6_merch_total = sprintf("=SUM(EC%s:EC%s)",$prev+1,$cnt-1);
                                    $_pnb6_ewt_total = sprintf("=SUM(ED%s:ED%s)",$prev+1,$cnt-1);
                                    $_pnb6_net_total = sprintf("=SUM(EE%s:EE%s)",$prev+1,$cnt-1);

                                    $_pnb12_amount_total = sprintf("=SUM(EG%s:EG%s)",$prev+1,$cnt-1);
                                    $_pnb12_merch_total = sprintf("=SUM(EH%s:EH%s)",$prev+1,$cnt-1);
                                    $_pnb12_ewt_total = sprintf("=SUM(EI%s:EI%s)",$prev+1,$cnt-1);
                                    $_pnb12_net_total = sprintf("=SUM(EJ%s:EJ%s)",$prev+1,$cnt-1);


                                    $_rcbc3_amount_total = sprintf("=SUM(EL%s:EL%s)",$prev+1,$cnt-1);
                                    $_rcbc3_merch_total = sprintf("=SUM(EM%s:EM%s)",$prev+1,$cnt-1);
                                    $_rcbc3_ewt_total = sprintf("=SUM(EN%s:EN%s)",$prev+1,$cnt-1);
                                    $_rcbc3_net_total = sprintf("=SUM(EO%s:EO%s)",$prev+1,$cnt-1);

                                    $_rcbc6_amount_total = sprintf("=SUM(EQ%s:EQ%s)",$prev+1,$cnt-1);
                                    $_rcbc6_merch_total = sprintf("=SUM(ER%s:ER%s)",$prev+1,$cnt-1);
                                    $_rcbc6_ewt_total = sprintf("=SUM(ES%s:ES%s)",$prev+1,$cnt-1);
                                    $_rcbc6_net_total = sprintf("=SUM(ET%s:ET%s)",$prev+1,$cnt-1);

                                    $_rcbc12_amount_total = sprintf("=SUM(EV%s:EV%s)",$prev+1,$cnt-1);
                                    $_rcbc12_merch_total = sprintf("=SUM(EW%s:EW%s)",$prev+1,$cnt-1);
                                    $_rcbc12_ewt_total = sprintf("=SUM(EX%s:EX%s)",$prev+1,$cnt-1);
                                    $_rcbc12_net_total = sprintf("=SUM(EY%s:EY%s)",$prev+1,$cnt-1);

                                    $_union3_amount_total = sprintf("=SUM(FA%s:FA%s)",$prev+1,$cnt-1);
                                    $_union3_merch_total = sprintf("=SUM(FB%s:FB%s)",$prev+1,$cnt-1);
                                    $_union3_ewt_total = sprintf("=SUM(FC%s:FC%s)",$prev+1,$cnt-1);
                                    $_union3_net_total = sprintf("=SUM(FD%s:FD%s)",$prev+1,$cnt-1);

                                    $_union6_amount_total = sprintf("=SUM(FF%s:FF%s)",$prev+1,$cnt-1);
                                    $_union6_merch_total = sprintf("=SUM(FG%s:FG%s)",$prev+1,$cnt-1);
                                    $_union6_ewt_total = sprintf("=SUM(FH%s:FH%s)",$prev+1,$cnt-1);
                                    $_union6_net_total = sprintf("=SUM(FI%s:FI%s)",$prev+1,$cnt-1);

                                    $_union12_amount_total = sprintf("=SUM(FK%s:FK%s)",$prev+1,$cnt-1);
                                    $_union12_merch_total = sprintf("=SUM(FL%s:FL%s)",$prev+1,$cnt-1);
                                    $_union12_ewt_total = sprintf("=SUM(FM%s:FM%s)",$prev+1,$cnt-1);
                                    $_union12_net_total = sprintf("=SUM(FN%s:FN%s)",$prev+1,$cnt-1);
                                     $sheet->row($cnt, function($row) {

 
                                        $row->setBackground('##f1c40f');

                                    });
                           
                                      $sheet->row($cnt,array(
                                        "DAILY SUB TOTAL",
                                         '' ,
                                        ' ',
                                        ' ',
                                        $_atm_total ,
                                        $_credit_card_total ,
                                        ' ',
                                        $_atm_amount_total ,
                                        $_atm_merch_total ,
                                        $_atm_net_total ,
                                        ' ',
                                        $_str_amount_total ,
                                        $_str_merch_total,
                                        $_str_ewt_total ,
                                        $_str_net_total ,
                                        ' ',
                                        $_jcb_amount_total ,
                                        $_jcb_merch_total ,
                                        $_jcb_ewt_total ,
                                        $_jcb_net_total ,
                                        ' ',
                                        $_bdo3_amount_total ,
                                        $_bdo3_merch_total ,
                                        $_bdo3_ewt_total ,
                                        $_bdo3_net_total ,
                                        ' ',
                                        $_bdo6_amount_total ,
                                        $_bdo6_merch_total ,
                                        $_bdo6_ewt_total ,
                                        $_bdo6_net_total ,
                                        ' ',
                                        $_bdo12_amount_total ,
                                        $_bdo12_merch_total ,
                                        $_bdo12_ewt_total ,
                                        $_bdo12_net_total ,
                                        ' ',
                                        $_bpi3_amount_total ,
                                        $_bpi3_merch_total ,
                                        $_bpi3_ewt_total ,
                                        $_bdo3_net_total ,
                                        ' ',
                                        $_bpi6_amount_total ,
                                        $_bpi6_merch_total ,
                                        $_bpi6_ewt_total ,
                                        $_bpi6_net_total ,
                                        ' ',
                                        $_bpi12_amount_total ,
                                        $_bpi12_merch_total ,
                                        $_bpi12_ewt_total ,
                                        $_bpi12_net_total ,
                                        ' ',
                                        $_citibank3_amount_total,
                                        $_citibank3_merch_total,
                                        $_citibank3_ewt_total,
                                        $_citibank3_net_total,
                                        ' ',
                                        $_citibank6_amount_total,
                                        $_citibank6_merch_total ,
                                        $_citibank6_ewt_total ,
                                        $_citibank6_net_total ,
                                        ' ',
                                        $_citibank12_amount_total ,
                                        $_citibank12_merch_total ,
                                        $_citibank12_ewt_total ,
                                        $_citibank12_net_total ,
                                        ' ',
                                        $_hsbc3_amount_total ,
                                        $_hsbc3_merch_total ,
                                        $_hsbc3_ewt_total,
                                        $_hsbc3_net_total ,
                                        ' ',
                                        $_hsbc6_amount_total ,
                                        $_hsbc6_merch_total,
                                        $_hsbc6_ewt_total ,
                                        $_hsbc6_net_total,
                                        ' ',
                                        $_hsbc12_amount_total,
                                        $_hsbc12_merch_total,
                                        $_hsbc12_ewt_total ,
                                        $_hsbc12_net_total ,
                                        ' ',
                                        $_east3_amount_total,
                                        $_east3_merch_total ,
                                        $_east3_ewt_total ,
                                        $_east3_net_total ,
                                        ' ',
                                        $_east6_amount_total ,
                                        $_east6_merch_total ,
                                        $_east6_ewt_total ,
                                        $_east6_net_total ,
                                        ' ',
                                        $_east12_amount_total ,
                                        $_east12_merch_total ,
                                        $_east12_ewt_total ,
                                        $_east12_net_total ,
                                        ' ',
                                        $_metro3_amount_total ,
                                        $_metro3_merch_total ,
                                        $_metro3_ewt_total ,
                                        $_metro3_net_total ,
                                        ' ',
                                        $_metro6_amount_total ,
                                        $_metro6_merch_total ,
                                        $_metro6_ewt_total ,
                                        $_metro6_net_total ,
                                        ' ',
                                        $_metro12_amount_total ,
                                        $_metro12_merch_total ,
                                        $_metro12_ewt_total ,
                                        $_metro12_net_total ,
                                        ' ',
                                        
                                        $_diner3_amount_total ,
                                        $_diner3_merch_total ,
                                        $_diner3_ewt_total ,
                                        $_diner3_net_total ,
                                        ' ',
                                        $_diner6_amount_total ,
                                        $_diner6_merch_total ,
                                        $_diner6_ewt_total ,
                                        $_diner6_net_total ,
                                        ' ',
                                        $_diner12_amount_total ,
                                        $_diner12_merch_total ,
                                        $_diner12_ewt_total ,
                                        $_diner12_net_total ,
                                        ' ',
                                        $_pnb3_amount_total ,
                                        $_pnb3_merch_total ,
                                        $_pnb3_ewt_total ,
                                        $_pnb3_net_total ,
                                        ' ',
                                        $_pnb6_amount_total ,
                                        $_pnb6_merch_total,
                                        $_pnb6_ewt_total ,
                                        $_pnb6_net_total ,
                                        ' ',
                                        $_pnb12_amount_total ,
                                        $_pnb12_merch_total ,
                                        $_pnb12_ewt_total ,
                                        $_pnb12_net_total ,
                                        ' ',
                                        $_rcbc3_amount_total,
                                        $_rcbc3_merch_total ,
                                        $_rcbc3_ewt_total ,
                                        $_rcbc3_net_total ,
                                        ' ',
                                        $_rcbc6_amount_total ,
                                       $_rcbc6_merch_total ,
                                        $_rcbc6_ewt_total ,
                                        $_rcbc6_net_total ,
                                        ' ',
                                        $_rcbc12_amount_total ,
                                        $_rcbc12_merch_total ,
                                        $_rcbc12_ewt_total ,
                                        $_rcbc12_net_total ,
                                        ' ',
                                        $_union3_amount_total ,
                                        $_union3_merch_total,
                                        $_union3_ewt_total ,
                                        $_union3_net_total ,
                                        
                                        ' ',
                                        $_union6_amount_total ,
                                        $_union6_merch_total ,
                                        $_union6_ewt_total ,
                                        $_union6_net_total ,
                                        ' ',
                                        $_union12_amount_total ,
                                        $_union12_merch_total ,
                                        $_union12_ewt_total,
                                        $_union12_net_total 
                                        ));
                           

                            



            });
        })->export('xls');
        
     



    }

}
