<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;


use App\Models\UserBranch;
use App\Models\CompanyBranch;
use App\Models\SaleSummary;




class DetailedSalesController extends Controller
{
    //


      public static function index(Request $request)
      {

        $date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        $branches  = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.sales.detailed_sales',compact('date_from','branches','date_to'));

    }


    public static function post(Request  $request) 
    {
    	set_time_limit(0);
        ini_set('memory_limit', '1G');
        $date_from      = $request->date_from;
        $date_to        = $request->date_to;
        $branch        =  CompanyBranch::where('branch_code',$request->branch)->first();

        $filename = "DETAILED DAILY SALES REPORT - ".$branch->branch;
     
        $sales = SaleSummary::detailDailySales($date_from,$date_to,$branch->branch_code);
        $first_date = " ";
        $last_date = date('d-M',strtotime(SaleSummary::getMinDate($date_from,$date_to,$branch->branch_code)));
      
    
       
        \Excel::create($filename, function($excel)  use ($branch,$date_from,$date_to,$sales,$first_date,$last_date){
            $excel->sheet("DETAILED DAILY SALES REPORT", function($sheet) use($date_from,$branch,$date_to,$sales,$first_date,$last_date) {
                            $sheet->setStyle(array(
                                'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  8
                                  
                                )
                            ));
                            $sheet->setWidth(array(
                                'A'     =>  30,
                                'B'     =>  20,
                                'C'     =>  20,
                                'D'     =>  20,
                                'E'     =>  5,
                                'F'     =>  20,
                                'G'     =>  20,
                                'H'     =>  20,
                                'I'     =>  20,
                                'J'     =>  5,
                                'K'     => 20,
                                'L'     => 20,
                                'M'     => 20,
                                'N'     => 5,
                                'O'     => 20,
                                'P'     => 20,
                                'Q'     => 20,
                                'R'     =>  5,
                                'S'     => 20,
                                'T'     => 20,
                                'U'     => 20,
                            ));
                           
                            $sheet->mergeCells('F5:I5');
                            $sheet->mergeCells('K5:M5');
                            $sheet->mergeCells('O5:Q5');
                            $sheet->mergeCells('S5:U5');
                            $sheet->setBorder('A5:D5', 'thin');
                            $sheet->setBorder('F5:I5', 'thin');
                       
                            $sheet->setBorder('K5:M5', 'thin');
                            $sheet->setBorder('O5:Q5', 'thin');
                            $sheet->setBorder('S5:U5', 'thin');


                            $sheet->setBorder('A6:D6', 'thin');
                            $sheet->setBorder('F6:I6', 'thin');
                    
                            $sheet->setBorder('K6:M6', 'thin');
                            $sheet->setBorder('O6:Q6', 'thin');
                            $sheet->setBorder('S6:U6', 'thin');

                            $sheet->cells('A5:U5', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });
                            $sheet->cells('A6:U6', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });

                            $sheet->row(1, array("PANDORA - ".$branch->branch));
                            $sheet->row(2, array("DETAILED DAILY SALES REPORT"));
                            $sheet->row(3, array("FOR THE PERIOD ".$date_from." to ".$date_to));
                        
                            $sheet->row(5,array('' ,'SALES INV','REF NO.','','','VAT EXEMPT TRANSACTIONS',' ',' ',' ',' ','CASH SALES',' ', ' ',' ','CHARGE SALES',' ',' ',' ','GRAND TOTAL'));

                            $sheet->row(6,array('DATE','NUMBER',' ','CUSTOMER NAME',' ','SENIOR CITIZEN','PWD','DIPLOMAT','TOTAL',' ', 'VAT IN','VAT EXEMPT','TOTAL',' ','VAT IN','VAT EXEMPT','TOTAL','','VAT IN' ,'VAT EXEMPT' ,'TOTAL'));
                             $cnt = 8;
                            $last = 0;
                             $index = 0;
                               $prev = 0;
                               $total_of_all = 0.00;
                            foreach ($sales as $data) {
                                $senior = ($data->discount_name == "SENIOR CITIZEN") ? $data->net_amount : 0 ;
                                $pwd = ($data->discount_name == "PWD") ? $data->net_amount : 0 ;
                                $diplomat = ($data->discount_name == "DIPLOMAT") ? $data->net_amount : 0 ;
                                $total_exempt = $senior + $pwd + $diplomat;
                                
                                $cash_vatin = 0;
                                $cash_vatex  = 0;
                                $cash_total = 0;

                                $charge_vatin = 0;
                                $charge_vatex  = 0;
                                $charge_total = 0;

                                $grand_vatin = 0;
                                $grand_vatex  = 0;
                                $grand_total = 0;
                                $super_grand_total = 0;
                                if($data->discount_name == "SENIOR CITIZEN" || $data->discount_name == "PWD" || $data->discount_name == "DIPLOMAT" ) {

                                    $cash_vatex   = $data->cash_sales;
                                    $charge_vatex = $data->charge_sales;


                                }

                                else {

                                    $cash_vatin   = $data->cash_sales;
                                    $charge_vatin = $data->charge_sales;

                                }

                                $cash_total   = $cash_vatin + $cash_vatex;
                                $charge_total = $charge_vatin + $charge_vatex;

                                $grand_vatin  = $cash_vatin +  $charge_vatin;
                                $grand_vatex  =  $charge_vatex + $cash_vatex;
                              
                                $super_grand_total = $super_grand_total + $grand_total;
                            if($data->local_time != "NO DATA") {
                                
                                 $first_date = date('d-M',strtotime($data->local_time));

                                if($first_date != $last_date && $index > 0) {
                                   
                                    $_vat_exempt_total = 0.00;
                                    $_cash_sales_total = 0.00;
                                    $_charge_sales_total = 0.00;
                                    $_grand_total = 0.00;

                                    if($index == 2) {

                                        $_charge_sales_total =  sprintf("=SUM(Q%s:Q%s)",8,$cnt-1);
                                        $_grand_total =  sprintf("=SUM(U%s:U%s)",8,$cnt-1);
                                        $_vat_exempt_total = sprintf("=SUM(I%s:I%s)",8,$cnt-1);
                                        $_cash_sales_total = sprintf("=SUM(M%s:M%s)",8,$cnt-1);
                                    }
                                    else {

                                        $_charge_sales_total =  sprintf("=SUM(Q%s:Q%s)",$prev+1,$cnt-1);
                                        $_grand_total =  sprintf("=SUM(U%s:U%s)",$prev+1,$cnt-1);
                                        $_vat_exempt_total = sprintf("=SUM(I%s:I%s)",$prev+1,$cnt-1);
                                        $_cash_sales_total = sprintf("=SUM(M%s:M%s)",$prev+1,$cnt-1);

                                    }






                                    $sheet->row($cnt, function($row) {

 
                                        $row->setBackground('##f1c40f');

                                    });

                                     $sheet->row($cnt,array(
                                        
                                              // $row->setBackground('#000000');
                                        
                                       

                                        
                                            "DAILY SUB TOTAL",'','',' ',' ',' ','','',$_vat_exempt_total,' ', ' ',' ',$_cash_sales_total,' ',' ',' ',$_charge_sales_total,'',' ' ,' ' ,$_grand_total
                                           
                                        

                                        ));

                                     $prev = $cnt;
                                       $cnt++;

                                }

                                // if($first_date == $last_date) {
                                    $sheet->row($cnt,array(
                                        

                                        
                                       

                                        
                                            date('d-M',strtotime($data->local_time)),
                                            $data->invoice_no,
                                            $data->ref_no,
                                            $data->customer_name,
                                            ' ',
                                            $senior,
                                            $pwd,
                                            $diplomat,
                                            $total_exempt,
                                            ' ',
                                            $cash_vatin,
                                            $cash_vatex,
                                            $cash_total,
                                            ' ',
                                            $charge_vatin,
                                            $charge_vatex,
                                            $charge_total,
                                            ' ',
                                            $grand_vatin,
                                            $grand_vatex,
                                            $grand_total,

                                            
                                        

                                        ));
                                    $index++;
                                // }
                    
                               
                                 $last =    $cnt++;
                                 $last_date =  $first_date;
                                 $total_of_all = $total_of_all + $grand_total;
                                }
                            



                            
                         }

                            $_vat_exempt_total = 0.00;
                            $_cash_sales_total = 0.00;
                            $_charge_sales_total = 0.00;
                            $_grand_total = 0.00;
                            $_charge_sales_total =  sprintf("=SUM(Q%s:Q%s)",$prev+1,$cnt-1);
                            $_grand_total =  sprintf("=SUM(U%s:U%s)",$prev+1,$cnt-1);
                            $_vat_exempt_total = sprintf("=SUM(I%s:I%s)",$prev+1,$cnt-1);
                            $_cash_sales_total = sprintf("=SUM(M%s:M%s)",$prev+1,$cnt-1);



                            $sheet->row($cnt, function($row) {

   
                                $row->setBackground('##f1c40f');

                            });
                           $sheet->row($cnt,array(
                                        
                                             
                                 "DAILY SUB TOTAL",'','',' ',' ',' ','','',$_vat_exempt_total,' ', ' ',' ',$_cash_sales_total,' ',' ',' ',$_charge_sales_total,'',' ' ,' ' ,$_grand_total
                            ));   
                           
                         $cnt++;
                         $cnt++;
                         $cnt++;
                         if($last > 0) {
                     
                            $b = sprintf("A%s:U%s",$cnt,$cnt);
                            $sheet->setBorder($b, 'thin');
                            $sheet->row($cnt,array('TOTAL','','',' ',' ',' ','','','',' ', ' ',' ','',' ',' ',' ','','',' ' ,' ' ,$total_of_all));
                        }
            });
        })->export('xls');
        
        



    }



}
