<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PostVoid;

class PostVoidController extends Controller
{
    public function index(Request $request){
    	$transactions = PostVoid::all();
    	return view('postvoid.index', compact('transactions'));
    }
}
