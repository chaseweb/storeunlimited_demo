<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SaleSummary;
use App\Models\UserBranch;
use App\Models\SaleDetail;
use App\Models\CompanyBranch;
use Auth;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

class ItemRankController extends Controller
{
    public function index(Request $request){
    	$date_from = date('m/d/Y');
    	$date_to = date('m/d/Y');
    	$companies = SaleSummary::getCompanies();
      $items = SaleDetail::itemranking($date_from, $date_to, $request);
      $globals = SaleDetail::getGlobalStatus();
      $campids = SaleDetail::getCampIds();
      $metals = SaleDetail::getMetals();
    	return view('itemrank.index', compact( 'companies', 'date_from', 'date_to', 'items','globals','campids','metals'));
    }

    public function store(Request $request){
    	$request->flash();
    	$date_from = $request->date_from;
    	$date_to = $request->date_to;
    	$sel_branches = $request->branch;

    	$companies = SaleSummary::getCompanies();
      $globals = SaleDetail::getGlobalStatus();
      $campids = SaleDetail::getCampIds();
      $metals = SaleDetail::getMetals();
      
        $items = SaleDetail::itemranking($date_from, $date_to, $request);

         $submit_type = $request->get('submit');
         
         if($submit_type == 1 ){

            return view('itemrank.index', compact( 'companies', 'sel_branches', 'date_from', 'date_to', 'items', 'globals', 'campids','metals'));
         }
          elseif ($submit_type == 2) {


        for ($i =   0;  $i  <   sizeof($items);   $i++)   {
            for ($j=$i+1;$j <  sizeof($items);  $j++)   {

                if  ($items[$i]->qty  <   $items[$j]->qty) {
                                $c  =   $items[$i];
                                $items[$i]    =   $items[$j];
                                $items[$j]    =   $c;
                    }
                }
        }


     
        
// as
            $writer = WriterFactory::create(Type::XLSX);
            $writer->openToBrowser("ITEM RANKING REPORT".'.xlsx');
            $style_header = (new StyleBuilder())           
               ->setFontSize(14)
               ->setFontName('Calibri')
               ->setFontColor(Color::BLACK)           
               ->build();   

            $style_details = (new StyleBuilder())           
               ->setFontSize(11)
               ->setFontName('Agency FB')
               ->setFontColor(Color::BLACK)           
               ->build();


            $writer->addRowWithStyle(array('Barcode','Item Code','Description','Global Status','Campaign Id','Metal','Qty','Total Amount'),$style_header);

                            foreach($items as $item){
                                   
                                $data[0] =  $item->barcode; 
                                $data[1] =  $item->itemcode; 
                                $data[2] =  $item->description;
                                $data[3] =  $item->global_status;
                                $data[4] =  $item->campaign_id;
                                $data[5] =  $item->metal;
                                $data[6] =  $item->qty; 
                                $data[7] =  number_format($item->net_amount,2);
                                $writer->addRowWithStyle($data,$style_details);
           
                            }

            $sheet = $writer->getCurrentSheet();
            $sheet->setName('ITEM RANKING REPORT');
            $writer->close();               

        }

    	
    }
}
