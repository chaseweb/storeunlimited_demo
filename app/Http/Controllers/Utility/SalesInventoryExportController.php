<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleSummary;
use App\Models\SalesInventory;
use App\Models\SalesInventoryExcel;
use App\Models\UserBranch;
use App\Models\ReturnExchangeItem;
use App\Models\ItemStock;
use Session;
use Auth;
use File;
use Alchemy\Zippy\Zippy;
use DB;
use App\Models\CompanyBranch;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

class SalesInventoryExportController extends Controller
{
    public function index(Request $request){
    	$date_from = date('m/d/Y');
    	$date_to = date('m/d/Y');
    	$branches = UserBranch::getUserBranch(Auth::user()->id);
    	return view('salesinventory.index', compact( 'branches', 'date_from', 'date_to'));
    }
    public function store(Request $request){                
        $request->flash();
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $sel_branches = $request->branches;
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        $export_type = $request->get('export');

        if($export_type == "1"){

            $files = SalesInventory::getFiles($date_from, $date_to, $request);

            if(empty($files)){
                Session::flash('flash_message', 'No record found!');
                Session::flash('flash_class', 'alert-danger');
                return view('salesinventory.index', compact( 'branches', 'date_from', 'date_to'));
            }else{
                $zippy = Zippy::load();
                
                $folder_path = storage_path().'/zipped/salesinventories/'.Auth::user()->id."_SaleInventory";

                if(!File::exists($folder_path)) {
                
                        File::makeDirectory($folder_path ,0755, true);
                    
                }

                $zip_path = $folder_path.'/'.Auth::user()->id."_SaleInventory.zip";

                File::delete($zip_path);
                // $with_files = false;

                $folders = [];
                foreach ($files as $file) {
                    $distination = $folder_path;
                    if($file->type == 1){
                        $folders['SALES/'.$file->file] = storage_path().'/uploads/sftp/'.$file->file;
                    }else{
                        $folders['INVENTORY/'.$file->file] = storage_path().'/uploads/sftp/'.$file->file;
                    }
                }
                $archive = $zippy->create($zip_path,$folders,true);
                return response()->download($zip_path);
            }
        }    
        if($export_type == "2"){

            $files = SalesInventoryExcel::getFiles($date_from, $date_to, $request);

            if(empty($files)){
                Session::flash('flash_message', 'No record found!');
                Session::flash('flash_class', 'alert-danger');
                return view('salesinventory.index', compact( 'branches', 'date_from', 'date_to'));
            }else{

                $zippy = Zippy::load();
                
                $folder_path = storage_path().'/zipped/salesinventories_excel/'.Auth::user()->id."_SaleInventoryExcel";

                if(!File::exists($folder_path)) {
                
                    
                        File::makeDirectory($folder_path,0755, true);
                    
                }

                $zip_path = $folder_path.'/'.Auth::user()->id."_SaleInventoryExcel.zip";
                // $zip_path = $folder_path.'/'."SalesInv".$date_from.'-'.$date_to.".zip";

                File::delete($zip_path);
                // $with_files = false;

                $folders = [];
                foreach ($files as $file) {
                    $distination = $folder_path;
                    if($file->type == 1){
                        $folders['SALES/'.$file->file] = storage_path().'/uploads/excel_upload/'.$file->file;
                    }else{
                        $folders['INVENTORY/'.$file->file] = storage_path().'/uploads/excel_upload/'.$file->file;
                    }
                }
               
                $archive = $zippy->create($zip_path,$folders,true);
                return response()->download($zip_path);    
            }
        }   

        if($export_type == "3"){
            ini_set('memory_limit', '-1');
            $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));
                
            if(($request->has('branches'))&& (!empty($request->branches)))  {
                    $_branches = $request->branches;
            } else {
                    
                    $_branches = [];
                    foreach ($branches as $key => $value) {
                        $_branches[] = $key; 
                        # code...
                    }
                     
            }


            $data = [];
            $items = SaleSummary::salesSummarPerItem($date_from,$date_to,$request);
            $folder_path = storage_path().'/files/'.date('Y-m-d-H-i-s');
            if(!File::exists($folder_path)) {
                
                    
                        File::makeDirectory($folder_path,0755, true);
                    
            }
            

            foreach ($items as $key => $item) {
                $revenue = 0.00;
                $net_qty =  $item->net_qty - ($item->return_qty + $item->refund_qty);
                $revenue = $item->net_amount - ($item->return_exchange_amount + $item->refund_amount);
                $revenue = round($revenue * 1.12,2);
                if(empty($net_qty)) { $net_qty = 0;}
                if(empty($revenue)) {$revenue = 0;}
                $data[$item->branch][$key] = ['itemcode'=>$item->itemcode,'qty' => $net_qty, 'revenue' => $revenue ];

            }
                
            foreach ($_branches as $key => $value) {
                
                    set_time_limit(0); 
                    $branch = CompanyBranch::where('branch_code',$value)->first();
                    $items = $data[$branch->branch];
                    
                    $filename = "SALES_".$branch->branch_code."_".$branch->branch."_FROM".$date_from."_TO".$date_to;

                    \Excel::create($filename, function($excel)  use ($items){
                                    $excel->sheet('Sheet1', function($sheet) use($items) {
                                         $sheet->fromArray($items, null, 'A1', false);
                                    });
                    })->store('xlsx',$folder_path);
            }
        }
    }
    public function sindex(Request $request)
    {
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);
        return view('salesinventory.sindex', compact( 'branches', 'date_from', 'date_to'));
    }
    public function sgenerate(Request $request)
    {
        $request->flash();
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $sel_branches = $request->branches;
        $branches = UserBranch::getUserBranch(Auth::user()->id);
        $companies = SaleSummary::getCompanies();

        // $export_type = $request->get('export');
        // $summaries = SaleSummary::salessummary2($date_from, $date_to, $request);
        $summaries = ItemStock::getBranchStocks($date_from, $date_to, $sel_branches);
        $submit_type = $request->get('export');
        $item = $request->keyword;

        if ($submit_type == 1){
            $data = [];

            foreach ($summaries as $key=>$summary) {

                $data[$key] = [
                    'Item Code' => $summary->itemcode,
                    'Qty'=> $summary->qty
                ]; 
            }
            \Excel::create('INVENTORY_SUMMARY_'.$date_from.'_'.$date_to.'', function($excel)  use ($data){
                $excel->sheet('Sheet1', function($sheet) use($data) {
                    $sheet->fromArray($data, null, 'A1', false);
                });
            })->download('xls');
        }
    }
}
