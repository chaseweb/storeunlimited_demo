<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleSummary;
use App\Models\SalesInventory;
use App\Models\SalesInventoryExcel;
use App\Models\UserBranch;
use App\Models\ReturnExchangeItem;
use App\Models\ItemStock;
use App\Models\CompanyBranch;
use App\Listeners\ExcelGenerated;
use Session;
use Auth;
use File;
use DB;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Alchemy\Zippy\Zippy;

class SalesInventorySummaryExportController extends Controller
{
    public function index(Request $request)
    {
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);
        return view('salesinventory.sindex', compact( 'branches', 'date_from', 'date_to'));
    }
    public function store(Request $request)
    {        
        set_time_limit(0);
        ini_set('memory_limit', -1);
        $request->flash();
        $date_from = str_replace('/', '_', $request->date_from);
        $date_to = str_replace('/', '_', $request->date_to);
        $sel_branches = $request->branches;
        $branches = UserBranch::getUserBranch(Auth::user()->id);
        $companies = SaleSummary::getCompanies();

        $items = ItemStock::getBranchStocks($request->date_from, $request->date_to, $sel_branches);        
        $submit_type = $request->get('export');
        $item = $request->keyword;        
        
        if ($submit_type == 1){
            $filename = "Branch_Inventory_Export_(Inventory)_".$date_from.'_'.$date_to;
            $count = 1;
            $savePath = storage_path('uploads/branch_inventory_export/'.$filename);
            if(!\File::exists($savePath)){
                mkdir($savePath, 0777, true);
            }            
            File::deleteDirectory($savePath, true);                       
            foreach(array_chunk($items, 5000) as $results){
                \Excel::create('BRANCH_INVENTORY_EXPORT('.$count.')', function($excel) use($results) {
                    $excel->sheet('INVENTORY_EXPORT', function($sheet) use($results) {
                        $sheet->appendRow(array(
                            'Item Code',
                            'Qty'
                        ));
                        foreach ($results as $result) {
                            $sheet->appendRow(array(
                                $result->itemcode,
                                $result->qty
                            ));
                        }
                    });
                })->store('xlsx',$savePath);
                $count++;
            }
            $zippy = Zippy::load();
            $files = \File::allFiles($savePath);
            if(count($files) > 0){
                $zip_path2 = $savePath.'/zipped/';
                foreach ($files as $file) {
                    $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
                }
                $folder_name = str_replace(":","_", $filename);
                $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

                if(!\File::exists($zip_path2)){
                    \File::makeDirectory($zip_path2,0777,true);
                }
                $archive = $zippy->create($zip_path,$folders,true);                
                return response()->download($zip_path);
            }else{
                return redirect()->back()->with('no_data','No Data');
            } 
        }
    }
    public function show(Request $request)
    {
    	$date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);
        return view('salesinventory.sindexs', compact( 'branches', 'date_from', 'date_to'));
    }
    public function generate(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', -1);

    	$request->flash();
        $date_from = str_replace('/', '_', $request->date_from);
        $date_to = str_replace('/', '_', $request->date_to);
        $sel_branches = $request->branches;
        $branches = UserBranch::getUserBranch(Auth::user()->id);
        $companies = SaleSummary::getCompanies();

        $items = ItemStock::getBranchSales($request->date_from, $request->date_to, $sel_branches);        
        $submit_type = $request->get('export');
        $item = $request->keyword;        
        if ($submit_type == 1){
            $filename = "Branch_Inventory_Export_(Sales)_".$date_from.'_'.$date_to;
            $count = 1;
            $savePath = storage_path('uploads/branch_isales_export/'.$filename);
            if(!\File::exists($savePath)){
                mkdir($savePath, 0777, true);
            }
            File::deleteDirectory($savePath, true);                       
            foreach(array_chunk($items, 3000) as $results){
                \Excel::create('BRANCH_SALES_EXPORT('.$count.')', function($excel) use($results) {
                    $excel->sheet('SALES_EXPORT', function($sheet) use($results) {
                        $sheet->appendRow(array(
                            'Item Code',
                            'Qty',
                            'Revenue'
                        ));
                        foreach ($results as $result) {
                            $sheet->appendRow(array(
                                $result->itemcode,
                                $result->qty,
                                $result->gross_amount
                            ));
                        }
                    });
                })->store('xlsx',$savePath);
                $count++;
            }
            $zippy = Zippy::load();
            $files = \File::allFiles($savePath);
            if(count($files) > 0){
                $zip_path2 = $savePath.'/zipped/';
                foreach ($files as $file) {
                    $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
                }
                $folder_name = str_replace(":","_", $filename);
                $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

                if(!\File::exists($zip_path2)){
                    \File::makeDirectory($zip_path2,0777,true);
                }
                $archive = $zippy->create($zip_path,$folders,true);
                return response()->download($zip_path);
            }else{
                return redirect()->back()->with('no_data','No Data');
            }               
        }
    }
}