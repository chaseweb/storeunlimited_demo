<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\UserBranch;
use App\Models\ManualInventory;
use Auth;
use Session;

class ManualInventoryController extends Controller
{
    public function index(){
    	$files = ManualInventory::getFiles();
    	return view('manualinventory.index', compact('files'));
    }

    public function create(){
    	$branches = UserBranch::getUserBranch2(Auth::user()->id);
    	return view('manualinventory.create', compact('branches'));
    }

    public function store(Request $request){
       
    	if($request->hasFile('files')){
    		$branch_id = $request->branch;
			$files = $request->file('files');
			$destinationPath = storage_path().'/uploads/manualinventory/';

			if (!\File::exists($destinationPath))
	        {
	            mkdir($destinationPath, 0755, true); 
	        }

			foreach ($files as $file) {
				if(!empty($file)){
					$original_file_name = $file->getClientOriginalName();
					$file->move($destinationPath,$original_file_name);
                    $newstr = substr($original_file_name, strlen($original_file_name) - 9);                    

					ManualInventory::create([
                        'branch_id' => $branch_id, 
                        'voucher_number' => substr($newstr,0,5),
                        'filename' => $original_file_name
                    ]);

				}
				
			}
			
		}

		Session::flash('flash_message', 'File successfully uploaded.');
        Session::flash('flash_class', 'alert-success');

        return redirect()->route("manualinventory.index");
    }

    public function show($id){
    	$file = ManualInventory::find($id);
    	$pathToFile = storage_path().'/uploads/manualinventory/'.$file->filename;
    	return response()->download($pathToFile);
    }
}
