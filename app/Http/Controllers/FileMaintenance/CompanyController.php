<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

use App\Models\Company;

class CompanyController extends Controller
{
    public function index(Request $request){
    	$companies = Company::search($request);
    	return view('companies.index',compact('companies'));
    }

    public function create(){
    	return view('companies.create');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'company_code' => 'required|max:100|unique:companies,company_code',
            'company' => 'required',
        ]);

        $company = new Company;
        $company->company_code = $request->company_code;
        $company->company = strtoupper($request->company);
        $company->save();

        Session::flash('flash_message', 'Company successfully added!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("companies.index");
    }

    public function edit($id){
        $company = Company::findOrFail($id);
        return view('companies.edit',compact('company'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'company_code' => 'required|max:100|unique:companies,company_code,'.$id,
            'company' => 'required',
        ]);

        $company = Company::findOrFail($id);

        $company->company_code = $request->company_code;
        $company->company = strtoupper($request->company);
        $company->update();

        Session::flash('flash_message', 'Company successfully updated!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("companies.index");
    }

    public function destroy(Request $request, $id){
        $company = Company::findOrFail($id);
        $company->delete();

        Session::flash('flash_message', 'Company successfully deleted!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("companies.index");
    }
}
