<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use DB;
use App\Models\SaleSummary;
use App\Models\ItemStock;
use App\Models\Company;
use App\Models\CompanyBranch;


class ZapController extends Controller
{
    public function index(){
        $branches = CompanyBranch::select('company_branches.id', 'company_branches.branch', 'company')
            ->join('companies', 'companies.id', '=', 'company_branches.company_id')
            ->orderBy('company')
            ->orderBy('branch')
            ->get();
    	return view('zap.index',compact('branches'));
    }

    public function store(Request $request){
    	$this->validate($request, [
            'branch' => 'required',
            'access_code' => 'required'
        ]);

        if($request->access_code == 'zap031988'){

            $branches = CompanyBranch::whereIn('id', $request->branch)->get();

            // dd($branches);
            if(!empty($branches)){
                foreach ($branches as $branch) {
                    // dd($branch->company);
                    DB::table('incoming_items')->where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->delete();
                    DB::table('outgoing_items')->where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->delete();
                    DB::table('purchase_orders')->where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->delete();
                    DB::table('refund_items')->where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->delete();
                    DB::table('return_exchange_items')->where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->delete();
                    DB::table('post_voids')->where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->delete();
                    

                    $sales = SaleSummary::where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->get();
                    foreach ($sales as $sale) {
                        DB::table('sale_details')->where('sale_summary_id', $sale->id)->delete();
                        $sale->delete();
                    }

                    $items = ItemStock::where('company_code', $branch->company->company_code)->where('branch_code',$branch->branch_code)->get();
                    foreach ($items as $item) {
                        DB::table('stock_histories')->where('item_stock_id', $item->id)->delete();
                        DB::table('stock_movements')->where('item_stock_id', $item->id)->delete();
                        $item->delete();
                    }
                }
                
            }
        	
        	Session::flash('flash_message', 'Transaction cleared');
        	Session::flash('flash_class', 'alert-success');
        }else{
        	Session::flash('flash_message', 'Invalid access code');
        	Session::flash('flash_class', 'alert-danger');
        }
   
        

        return redirect()->route("zap.index");
    }
}
