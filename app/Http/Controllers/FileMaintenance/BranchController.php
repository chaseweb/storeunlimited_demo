<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CompanyBranch;
use App\Models\Company;
use Session;

class BranchController extends Controller
{
    public function index(Request $request){
    	$branches = CompanyBranch::search($request);
    	return view('branches.index',compact('branches'));
    }

    public function create(){
    	$companies = Company::getList();
    	return view('branches.create', compact('companies'));
    }

    public function store(Request $request){
    	$this->validate($request, [
            'company_id' => 'required|unique_with:company_branches,branch_code',
            'branch_code' => 'required|max:100',
            'branch' => 'required',
            'area' => 'required',
        ]);

        $branch = new CompanyBranch;
        $branch->company_id = $request->company_id;
        $branch->branch_code = $request->branch_code;
        $branch->branch = strtoupper($request->branch);
        $branch->area = str_replace(",", "", $request->area);
        $branch->save();

        Session::flash('flash_message', 'Company Branch successfully added!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("branches.index");
    }

    public function edit($id){
        $branch = CompanyBranch::findOrFail($id);
        $companies = Company::getList();
        return view('branches.edit',compact('branch', 'companies'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'company_id' => 'required|unique_with:company_branches,branch_code,'.$id,
            'branch_code' => 'required|max:100',
            'branch' => 'required',
            'area' => 'required',
        ]);

        $branch = CompanyBranch::findOrFail($id);

        $branch->company_id = $request->company_id;
        $branch->branch_code = $request->branch_code;
        $branch->branch = strtoupper($request->branch);
        $branch->area = str_replace(",", "", $request->area);
        $branch->save();

        Session::flash('flash_message', 'Company Branch successfully updated!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("branches.index");
    }

    public function destroy(Request $request, $id){
        $branch = CompanyBranch::findOrFail($id);
        $branch->delete();

        Session::flash('flash_message', 'Company Branch successfully deleted!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("branches.index");
    }
}
