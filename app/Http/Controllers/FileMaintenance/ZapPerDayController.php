<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use DB;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\ItemStock;
use App\Models\Company;
use App\Models\CompanyBranch;
use App\Models\IncomingItem;
use App\Models\OutgoingItem;
use App\Models\PurchaseOrder;
use App\Models\RefundItem;
use App\Models\ReturnExchangeItem;
use App\Models\PostVoid;

class ZapPerDayController extends Controller
{
    public function index()
    {
    	$date_from = date('m-d-Y');
    	$date_to = date('m-d-Y');
        $branches = CompanyBranch::select('company_branches.id', 'company_branches.branch', 'company')
            ->join('companies', 'companies.id', '=', 'company_branches.company_id')
            ->orderBy('company')
            ->orderBy('branch')
            ->get();
    	return view('zap_per_day.index',compact('branches', 'date_from', 'date_to'));
    }
    public function store(Request $request)
    {
    	$this->validate($request, [
            'branch' => 'required',
            'access_code' => 'required'
        ]);
        if($request->access_code == 'zap031988'){
        	$from = date('Y-m-d', strtotime($request->date_from));
            $to = date('Y-m-d', strtotime($request->date_to));
	        $date_from = new \DateTime($from);
	        $date_to = date('Y-m-d H:i:s', strtotime('+23 hours + 59 minutes',strtotime($to)));

        	$branches = CompanyBranch::whereIn('id', $request->branch)->get();
    		foreach ($branches as $branch) {
    			$sales = SaleSummary::getIds($date_from, $date_to, $branch->branch_code);
    			if(!empty($sales)){
	    			foreach ($sales as $sale) {
	    				$details = SaleDetail::ZapPerDay($sale->id);
	    				if(!empty($details)){
	    					foreach($details as $detail) {
	    						$detail->delete();
	    					}
	    				}
	    				$sale->delete();
	    			}					

					Session::flash('flash_message', 'Transaction cleared');
    				Session::flash('flash_class', 'alert-success');
				}else{
					Session::flash('flash_message', 'No Data Found');
        			Session::flash('flash_class', 'alert-danger');
				}
    		}
    	}else{
    		Session::flash('flash_message', 'Invalid access code');
        	Session::flash('flash_class', 'alert-danger');
    	}
    	return redirect()->route("zap_per_day.index");
    }
    // $incomings = IncomingItem::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from);
	// $outgoings = OutgoingItem::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from);
	// $purchases = PurchaseOrder::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from);
	// $refunds = RefundItem::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from, $date_to);
	// $refund_exs = ReturnExchangeItem::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from, $date_to);
	// $voids = PostVoid::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from, $date_to);
	// $items = ItemStock::ZapPerDay($branch->company->company_code, $branch->branch_code, $date_from, $date_to);
}
