<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use App\Role;
use App\Models\CompanyBranch;
use App\Models\UserBranch;
use Auth; 

class UserController extends Controller
{
    public function index(Request $request){
    	$users = User::all();
    	return view('users.index',compact('users'));
    }

    public function create(){
    	$roles = Role::getList();
    	return view('users.create', compact('roles'));
    }

    public function store(Request $request){
    	$this->validate($request, [
            'name' => 'required|unique:users',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'same:password',
            // 'role' => 'required|integer|min:1'
        ]);
        
        // $role = Role::findOrFail($request->role);

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $user->active = 1;
        $user->save();


        // \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // $user->roles()->attach($role);
        // \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Session::flash('flash_message', 'User successfully added.');
        Session::flash('flash_class', 'alert-success');

        return redirect()->route("users.index");
    }

    public function edit($id){
        $user = User::findOrFail($id);
        $roles = Role::getList();
        return view('users.edit', compact('roles', 'user'));
    }

    public function update(Request $request, $id){
        $user = User::findOrFail($id);

        $this->validate($request, [
            'username' => 'required|unique:users,username,'.$id,
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'role' => 'required|integer|min:1'
        ]);

    
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->active = $request->active;
        $user->update();



        $role = Role::findOrFail($request->role);

        $role_user = \DB::table('role_user')->where('user_id',$id);
        $role_user->delete(); 

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $user->roles()->attach($role);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Session::flash('flash_message', 'User successfully updated.');
        Session::flash('flash_class', 'alert-success');

        return redirect()->route("users.index");
    }

    public function branch($id){
        $user = User::findOrFail($id);
    	$branches = CompanyBranch::all();
        $selected_branch = UserBranch::getBranch($id);
    	return view('users.branch', compact('branches', 'user', 'selected_branch'));
    }

    public function storebranch(Request $request, $id){
        UserBranch::addBranch($id, $request);

        Session::flash('flash_message', 'User branches successfully updated.');
        Session::flash('flash_class', 'alert-success');

        return redirect()->back();
    }

    public function updatepassword(Request $request, $pass){

        $this->validate($request, [            
            'password' => 'required|min:6',
            'password_confirmation' => 'same:password',
            // 'role' => 'required|integer|min:1'
        ]);

        $user = Auth::user();
        $user->password = \Hash::make($request->password);
        $user->update();

        Session::flash('flash_message', 'Password successfully updated.');
        Session::flash('flash_class', 'alert-success');
        return redirect('users/changepassword');
    }

    public function show($id){
        return view('users.changepassword');
    }
}
