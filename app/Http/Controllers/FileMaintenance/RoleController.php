<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Role;

class RoleController extends Controller
{
    public function index(Request $request){
    	$roles = Role::search($request);
    	return view('roles.index',compact('roles'));
    }

    public function create(){
    	return view('roles.create');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'name' => 'required|max:100|unique:roles,name',
            'display_name' => 'required',
        ]);

        $role = new Role();
        $role->name = strtolower($request->name);
        $role->display_name = strtoupper($request->display_name);
        $role->description = $request->description;
        $role->save();

        Session::flash('flash_message', 'Role successfully added!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("roles.index");
    }

    public function edit($id){
        $role = Role::findOrFail($id);
        return view('roles.edit',compact('role'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|max:100|unique:roles,name,'.$id,
            'display_name' => 'required',
        ]);

        $role = Role::findOrFail($id);

       	$role->name = $request->name;
        $role->display_name = strtoupper($request->display_name);
        $role->description = $request->description;
        $role->update();

        Session::flash('flash_message', 'Role successfully updated!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("roles.index");
    }


    public function destroy(Request $request, $id){
        $role = Role::findOrFail($id); // Pull back a given role
        dd($role);

		$role->delete();

        Session::flash('flash_message', 'Role successfully deleted!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("roles.index");
    }
}
