<?php

namespace App\Http\Controllers\InventoryReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder;

class PurchaseOrderController extends Controller
{
    //  /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function index(Request $request){
    	
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');

        $items = PurchaseOrder::search($date_from, $date_to ,$request);        

        $total_qty = 0; 

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }     

    	return view('purchase.index', compact('items','total_qty','date_to','date_from'));
    }

    public function store(Request $request){

        $sdate_from = date('Y-m-d',strtotime($request->get('date_from')));
        $sdate_to =  date('Y-m-d',strtotime($request->get('date_to')));

        $date_from = $request->get('date_from');
        $date_to = $request->get('date_to');
        
        $items = PurchaseOrder::search($sdate_from, $sdate_to ,$request);        
                
        $total_qty = 0; 

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }     

        return view('purchase.index', compact('items','total_qty','date_to','date_from'));
    }
}
