<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoucherNumberOnManualInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_inventories', function (Blueprint $table) {
            $table->string('voucher_number')->after('branch_id')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_inventories', function (Blueprint $table) {
             $table->dropColumn(['voucher_number']);
        });
    }
}
