<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalesManOnSaleSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_summaries', function (Blueprint $table) {
            $table->string('sales_man')->after('post_void')->nullable();
            $table->decimal('retex_amount', 12,3)->after('total_item_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_summaries', function (Blueprint $table) {
            $table->dropColumn(['sales_man', 'retex_amount']);
        });
    }
}
