<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('c_company_name')->index();
            $table->string('c_branch_name')->index();
            $table->string('c_terminal_no')->index();
            $table->string('coupon_code');
            $table->string('c_cahier');
            $table->datetime('c_date_time');

            $table->string('redeemed_by');
            $table->string('r_cashier');
            $table->string('r_company_name')->index();
            $table->string('r_branch_name')->index();
            $table->string('r_terminal_no')->index();
            $table->string('r_transaction_no')->index();
            $table->datetime('r_datetime');
            $table->boolean('redeemed');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupons');
    }
}
