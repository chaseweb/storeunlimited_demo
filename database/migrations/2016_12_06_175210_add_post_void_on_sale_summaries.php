<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostVoidOnSaleSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_summaries', function (Blueprint $table) {
            $table->boolean('post_void')->after('local_time')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_summaries', function (Blueprint $table) {
            $table->dropColumn(['post_void']);
        });
    }
}
