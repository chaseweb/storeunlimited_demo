<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInventoryExcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_inventory_excels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->string('branch_code');
            $table->string('file');
            $table->date('transact_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_inventory_excels');
    }
}
