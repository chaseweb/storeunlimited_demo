<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_summary_id')->unsigned();
            $table->foreign('sale_summary_id')->references('id')->on('sale_summaries');
            $table->string('barcode');
            $table->string('itemcode');
            $table->string('description');
            $table->decimal('cost', 12,3);
            $table->decimal('srp', 12,3);
            $table->integer('qty'); 
            $table->decimal('gross_amount', 12,3);
            $table->decimal('net_amount', 12,3);
            $table->decimal('discount', 12,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_details');
    }
}
