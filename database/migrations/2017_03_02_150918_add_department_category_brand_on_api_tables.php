<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentCategoryBrandOnApiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });

        Schema::table('incoming_items', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });

        Schema::table('return_exchange_items', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });

        Schema::table('refund_items', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });

        Schema::table('item_stocks', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });

        Schema::table('sale_details', function (Blueprint $table) {
            $table->string('department')->after('barcode')->nullable();          
            $table->string('category')->after('department')->nullable();
            $table->string('brand')->after('category')->nullable();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });

        Schema::table('incoming_items', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });

        Schema::table('return_exchange_items', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });

        Schema::table('refund_items', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });

        Schema::table('item_stocks', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });

        Schema::table('sale_details', function (Blueprint $table) {
             $table->dropColumn(['department']);
             $table->dropColumn(['category']);
             $table->dropColumn(['category']);
        });
    }
}
