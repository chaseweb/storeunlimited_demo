<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyCodeOnSaleSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_summaries', function (Blueprint $table) {
            $table->string('company_code')->nullable()->after('id');
            $table->string('branch_code')->nullable()->after('company_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_summaries', function (Blueprint $table) {
            $table->dropColumn(['company_code', 'branch_code']);
        });
    }
}
