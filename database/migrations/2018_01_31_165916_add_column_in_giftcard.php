<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInGiftcard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('gift_cards', function (Blueprint $table) {
                  $table->string('sale_summary_id')->after('id');                  
            $table->string('gift_name')->after('company_code');
              $table->string('amount')->after('transaction_no');        
             $table->string('account_name')->after('gift_name');
             $table->string('approved_no')->after('account_name');
             $table->boolean('corporate')->after('approved_no');
          

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

          Schema::table('gift_cards', function (Blueprint $table) {

                  $table->dropColumn(['account_name']);
                  $table->dropColumn(['approved_no']);
                   $table->dropColumn(['corporate']);
                    
             $table->dropColumn(['sale_summary_id']);
            $table->dropColumn(['gift_name']);

             });
    }
}
