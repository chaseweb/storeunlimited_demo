<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name')->index();
            $table->string('branch_name')->index();
            $table->string('terminal_no')->index();
            $table->string('user')->index();
            $table->string('incoming_no')->index();
            $table->string('outgoing_no')->index();
            $table->datetime('local_time');
            $table->string('barcode')->index();
            $table->string('itemcode')->index();
            $table->string('description')->index();
            $table->integer('qty');
            $table->decimal('price', 12,3);
            $table->decimal('amount', 12,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incoming_items');
    }
}
