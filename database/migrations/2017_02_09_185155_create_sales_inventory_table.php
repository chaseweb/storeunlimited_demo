<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\SalesInventory;

class CreateSalesInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('sales_inventories', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('type');
        //     $table->string('branch_code');
        //     $table->string('file');
        //     $table->date('transact_date');
        //     $table->timestamps();
        // });

        // $files = File::allFiles(storage_path().'/uploads/sftp');

        // foreach ($files as $file)
        // {   
        //     $info = pathinfo($file);
        //     $type = 1;
        //     $filename = $info['filename'];
        //     if(substr($filename,0,9) == "INVENTORY" ){
        //         $type = 2;
        //         $branch_code = substr($filename,12,4);
        //         $transact_date = substr($filename,17,4)."-".substr($filename,21,2)."-".substr($filename,23,2);
        //     }else{
        //         $branch_code = substr($filename,8,4);
        //         $transact_date = substr($filename,13,4)."-".substr($filename,17,2)."-".substr($filename,19,2);
        //     }
        //     // dd($transact_date);

        //     SalesInventory::firstOrCreate(['type' => $type, 
        //         'branch_code' => $branch_code, 
        //         'file' => $filename.".csv",
        //         'transact_date' => $transact_date]);

        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('sales_inventories');
    }
}
