<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueCodeOnOutgoingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->string('company_code')->nullable()->after('id');
            $table->string('branch_code')->nullable()->after('company_name');
            $table->string('unique_code')->nullable()->after('pouch_no');
            $table->string('to_company_code')->nullable()->after('terminal_no');
            $table->string('to_company')->nullable()->after('to_company_code');
            $table->string('to_branch_code')->nullable()->after('to_company');
            $table->string('to_branch')->nullable()->after('to_branch_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->dropColumn(['unique_code', 'to_company_code', 'to_company', 'to_branch_code', 'to_branch', 'company_code', 'branch_code']);
        });
    }
}
