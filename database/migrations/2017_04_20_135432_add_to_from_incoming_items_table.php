<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToFromIncomingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('incoming_items', function (Blueprint $table) {
          

            $table->string('from_company_code')->after('terminal_no')->nullable();
            $table->string('from_company')->after('from_company_code')->nullable();
            $table->string('from_branch_code')->after('from_company')->nullable();
            $table->string('from_branch')->after('from_branch_code')->nullable();     

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('incoming_items', function (Blueprint $table) {
            
             $table->dropColumn(['from_company_code']);
             $table->dropColumn(['from_company']);
             $table->dropColumn(['from_branch_code']);
             $table->dropColumn(['from_branch']);

        });
    }
}
