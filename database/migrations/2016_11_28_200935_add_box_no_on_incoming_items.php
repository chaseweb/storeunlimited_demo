<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoxNoOnIncomingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incoming_items', function (Blueprint $table) {
            $table->string('box_no')->nullable()->after('amount');
            $table->string('pouch_no')->nullable()->after('box_no');
            $table->string('unique_code')->nullable()->after('pouch_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incoming_items', function (Blueprint $table) {
            $table->dropColumn(['box_no', 'pouch_no', 'unique_code']);
        });
    }
}
