<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostVoidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_voids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code')->index();
            $table->string('company_name')->index();
            $table->string('branch_code')->index();
            $table->string('branch_name')->index();
            $table->string('terminal_no')->index();
            $table->string('terminal_code')->index();
            $table->string('user')->index();
            $table->string('transaction_no')->index();
            $table->datetime('local_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_voids');
    }
}
