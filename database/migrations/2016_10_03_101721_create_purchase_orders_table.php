<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name')->index();
            $table->string('branch_name')->index();
            $table->string('supplier')->index();
            $table->string('terminal_no')->index();
            $table->string('user')->index();
            $table->string('po_no')->index();
            $table->string('incoming_no')->index();
            $table->datetime('local_time');
            $table->string('barcode')->index();
            $table->string('itemcode')->index();
            $table->string('description')->index();
            $table->integer('qty');
            $table->decimal('price', 12,3);
            $table->decimal('amount', 12,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_orders');
    }
}
