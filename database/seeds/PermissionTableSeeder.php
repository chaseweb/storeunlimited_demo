<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('permissions')->truncate();

		DB::statement("INSERT INTO permissions (id, name, display_name, description) VALUES
			(1, 'itemmasterfile' , 'Item Masterfile', 'Item Masterfile Access'),
            (2, 'companymaintenance' , 'Company Maintenance', 'Company Maintenance Access'),
            (3, 'branchmaintenance' , 'Branch Maintenance', 'Branch Maintenance Access'),
            (4, 'rolemaintenance' , 'Role Maintenance', 'Role Maintenance Access'),
            (5, 'usermaintenance' , 'User Maintenance', 'User Maintenance Access');");
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
