<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'active' => '1',
            'email' => 'admin@chasetech.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'ADMINISTRATOR',
            'description' => 'Web Administrator'
        ]);

        DB::table('role_user')->truncate();

        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '1'
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
