<?php

use Illuminate\Database\Seeder;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\ItemStock;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;


class FixSaleSummaryDetails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sales = SaleSummary::all();
        foreach ($sales as $sale) {
        	$details = SaleDetail::where('sale_summary_id', $sale->id)->get();

        	if(count($details) == 0){
        		$filePath = storage_path().'/uploads/sales/sales' . $sale->transaction_no.'.txt';
        		echo $sale->transaction_no .PHP_EOL;
		        DB::beginTransaction();
		        try {
		            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
		            $reader->setFieldDelimiter('|');
		            $reader->open($filePath);
		            foreach ($reader->getSheetIterator() as $sheet) {
		                $last_id = 0;
		                $transaction_no = 0;
		                foreach ($sheet->getRowIterator() as $row) {
		                    
		                    $company_code = str_pad($row[1], 4, "0", STR_PAD_LEFT);
		                    $branch_code = $company_code.str_pad($row[3], 4, "0", STR_PAD_LEFT);
		                    $terminal_code = $branch_code.str_pad($row[5], 2, "0", STR_PAD_LEFT);

		                    if($row[0] == 'H'){
		                        if(count($row) == 24){
		                            $data['company_code'] = $company_code;
		                            $data['branch_code'] = $branch_code;
		                            $data['terminal_code'] = $terminal_code;
		                            $data['transaction_no'] = $row[6];
		                            // dd($data);
		                            if(SaleSummary::entryExist($data)){

		                                $sale = SaleSummary::where('company_code', $data['company_code'])
								            ->where('branch_code', $data['branch_code'])
								            ->where('terminal_code', $data['terminal_code'])
								            ->where('transaction_no', $data['transaction_no'])
								            ->first();

		                                $last_id = $sale->id;
		                                $transaction_no = $sale->transaction_no;
		                                $new = true;
		                            }else{
		                                $new = false;
		                            }
		                        }
		                        
		                    }

		                    if($new){
		                        if($row[0] == 'D'){
		                            $data['sale_summary_id'] = $last_id;
		                            
		                            $data['company_code'] = $company_code;
		                            $data['company_name'] = $row[2];
		                            $data['branch_code'] = $branch_code;
		                            $data['branch_name'] = $row[4];
		                            $data['ctr'] = $row[5];
		                            $data['barcode'] = $row[6];
		                            $data['itemcode'] = $row[7];
		                            $data['description'] = $row[8];
		                            $data['cost'] = $row[9];
		                            $data['srp'] = $row[10];
		                            $data['qty'] = $row[11];
		                            $data['gross_amount'] = $row[12];
		                            $data['net_amount'] = $row[13];
		                            $data['discount'] = $row[14];

		                            
		                            $data['movement_description'] = 'Sales Entry';
		                            $data['move_ref'] = $transaction_no;  
		                            $data['purpose'] = 'Sales Entry';

		                            $detail = SaleDetail::detailsExist($data);
		                            if(empty($detail)){
		                                SaleDetail::create([
		                                    'sale_summary_id' => $data['sale_summary_id'],
		                                    'ctr' => $data['ctr'],
		                                    'barcode' => $data['barcode'],
		                                    'itemcode' => $data['itemcode'],
		                                    'description' => $data['description'],
		                                    'cost' => $data['cost'],
		                                    'srp' => $data['srp'],
		                                    'qty' => $data['qty'],
		                                    'gross_amount' => $data['gross_amount'],
		                                    'net_amount' => $data['net_amount'],
		                                    'discount' => $data['discount']]);

		                                ItemStock::removeStocks($data);
		                            }                      
		                        }
		                    }   
		                }
		            }
		            $reader->close();
		            DB::commit();
		            // return response()->json(array('msg' => 'file uploaded', 'status' => 0));
		        } catch (Exception $e) {

		            DB::rollback();
		            // return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
		        }
        	}

        }
    }
}
