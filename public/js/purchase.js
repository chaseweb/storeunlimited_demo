
$(document).ready(function(){

	// function updatetype(){
	// 	$.ajax({
	// 		type: "POST",
	// 		data: {branches: getSelectedValues($('select#branch :selected'))},
	// 		url: hostname +"/api/upselltype",
	// 		success: function(data){
	// 			select = $('select#type');
	// 			select.empty();
	// 			$.each(data.selection, function(i, text) {
	// 				var sel_class = '';
	// 				if($.inArray( i,types) > -1){
	// 					sel_class = 'selected="selected"';
	// 				}
	// 				$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
	// 			});
	// 			select.multiselect('rebuild');
	// 	   	},
	// 	   	error: function(){
	// 	   		$.alert({
	// 			    title: 'Alert!',
	// 			    type: 'red',
	// 			    content: 'Type not found!',
	// 			});
	// 	   	}
	// 	});
	// }

	// updatetype();	


	// $('select#branch').multiselect({
	// 	maxHeight: 200,
	// 	includeSelectAllOption: true,
	// 	enableCaseInsensitiveFiltering: true,
	// 	enableFiltering: true,
	// 	onDropdownHide: function(event) {
	// 		updatetype();
	// 	}
	// });

	// $('select#type').multiselect({
	// 	maxHeight: 200,
	// 	includeSelectAllOption: true,
	// 	enableCaseInsensitiveFiltering: true,
	// 	enableFiltering: true		
	// });	

	$("#date_from, #date_to").mask("99/99/9999",{placeholder:"__/__/____"});
	$("#date_from").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });

    $("#date_to").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });
});
