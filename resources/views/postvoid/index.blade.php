@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Post Void Transaction Report</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>User</th>
                                        <th>Transaction #</th>
                                        <th>Date Time</th>
                                        <th>Posting Time</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @if($transactions->count() > 0)
                                    @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{ $transaction->company_name }}</td>
                                        <td>{{ $transaction->branch_name }}</td>
                                        <td>{{ $transaction->terminal_no }}</td>
                                        <td>{{ $transaction->user }}</td>
                                        <td>{{ $transaction->transaction_no }}</td>
                                        <td>{{ $transaction->local_time }}</td>
                                        <td>{{ $transaction->created_at }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="7">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection