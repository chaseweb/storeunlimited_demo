@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Gift Card Lists</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                        <a class="btn btn-success" href="{{ route('giftcards.create')}}">Upload Gift Cards</a>
                    </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Serial No</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>Transaction #</th>
                                        <th>User</th>
                                        <th>Redeemed By</th>
                                        <th>Date Redeemed</th>
                                        <th>Last Update</th>
                                        
                                    </tr>
                                </thead>


                                <tbody>
                                    @if($giftcards->count() > 0)
                                    @foreach($giftcards as $giftcard)
                                    <tr>
                                        <td>{{ $giftcard->company }}</td>
                                        <td>{{ $giftcard->serial_no }}</td>
                                        <td>{{ $giftcard->start_date }}</td>
                                        <td>{{ $giftcard->end_date }}</td>
                                        <td>{{ $giftcard->branch_name }}</td>
                                        <td>{{ $giftcard->terminal_no }}</td>
                                        <td>{{ $giftcard->transaction_no }}</td>
                                        <td>{{ $giftcard->user }}</td>
                                        <td>{{ $giftcard->member }}</td>
                                        <td>{{ $giftcard->local_time }}</td>
                                        <td>{{ $giftcard->updated_at }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="15">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection