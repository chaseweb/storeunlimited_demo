@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User Lists</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <a class="btn btn-success" href="{{ route('users.create')}}">New User</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Fullname</th>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                    <th colspan="3" class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($users->count() > 0)
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td></td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="{{ route('users.edit', $user->id)}}">Edit</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="{{ route('users.branch', $user->id)}}">Map Branch</a>
                                    </td>
                                    <td>
                                        {!! Form::open(array('route' => array('companies.destroy', $user->id), 'method' => 'DELETE')) !!}
                                            <button type="submit" class="btn btn-danger btn-xs confirm">Delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection