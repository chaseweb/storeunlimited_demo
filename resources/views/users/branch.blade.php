@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>


        <div class="row">
            {!! Form::open(array('route' => ['users.storebranch', $user->id],'class' => 'form-horizontal form-label-left')) !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Branch Lists</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div>
                        <a class="btn btn-primary" href="{{ route('users.index')}}">Cancel</a>
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:10px;"></th>
                                    <th>Company Name</th>
                                    <th>Branch Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($branches->count() > 0)
                                @foreach($branches as $branch)
                                <tr>
                                    <td>
                                        {{ Form::checkbox('branch[]', $branch->id, (in_array($branch->id, $selected_branch) ? true : false)) }}
                                    </td>
                                    <td>{{ $branch->company->company }} </td>
                                    <td>{{ $branch->branch }} </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection