@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Branch Lists</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <a class="btn btn-success" href="{{ route('branches.create')}}">New Branch</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Company Code</th>
                                    <th>Company Name</th>
                                    <th>Branch Code</th>
                                    <th>Branch Name</th>
                                    <th>Area (m)</th>
                                    <th colspan="2" class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($branches->count() > 0)
                                @foreach($branches as $branch)
                                <tr>
                                    <td>{{ $branch->company->company_code }}</td>
                                    <td>{{ $branch->company->company }}</td>
                                    <td>{{ $branch->branch_code }}</td>
                                    <td>{{ $branch->branch }}</td>
                                    <td>{{ $branch->area }}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="{{ route('branches.edit', $branch->id)}}">Edit</a>
                                    </td>
                                    <td>
                                        {!! Form::open(array('route' => array('branches.destroy', $branch->id), 'method' => 'DELETE')) !!}
                                            <button type="submit" class="btn btn-danger btn-xs confirm">Delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection