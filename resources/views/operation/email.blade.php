<div class="form-group">
	<h2>Hi {!!$fullname!!}</h2>	
</div>
<div class="form-group">
	<p>Thank you for Signing with our parking reservation system.</p>
	<p>Please Click the link below to complete your registration.</p>
	<p><a href="{!! $url !!}/{!! $code !!}"></a>{!! $url !!}/{!! $code !!}</p>
	<p>Thank you,</p>
</div>
<div><h2>Parking Administrator</h2></div>
