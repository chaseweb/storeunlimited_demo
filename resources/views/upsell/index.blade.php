@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')    

    @if(isset($sel_types))
    var types = <?php echo json_encode($sel_types); ?>;
    @else
    var types = [];
    @endif

@endpush

@push('scripts')
    <script src="{{ asset("js/upsell.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">              
            </div>
            <div class="clearfix"></div>

            {!! Form::open(array('route' => array('upsell.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
              
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">
                        <div class="row">                                                    
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="branch">Branch Name</label>
                                    {!! Form::select('branch[]',  $branches, null, array('id' => 'branch', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT BRANCH')) !!}
                                </div>                                    
                            </div>    
                             <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="branch">Type</label>
                                    {!! Form::select('type[]',  $sel_types, null, array('id' => 'type', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A TYPE...')) !!}
                                </div>                                    
                            </div>                                   
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="date_from">Date From</label>
                                    {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                </div>                                    
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="date_to">Date To</label>
                                     {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                </div>                                
                            </div>             
                        </div>
                        <div class="row">                                
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="keywords">Search</label>
                                    {!! Form::text('keyword',null,array('class' => 'form-control', 'id' => 'keyword')) !!}
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success">Process</button>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            {!! Form::close() !!}
            <div class="clearfix"></div>            
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Upsell Report</h2>
                            <div class="pull-right">
                                <h3>Total Qty: {!! $total_qty !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Branch Name</th>
                                        <th>Transaction #</th>
                                        <th>Description</th>
                                        <th>Quantity</th>         
                                        <th>Transaction Date</th>                             
                                    </tr>
                                </thead>
                                <tbody>                                   
                                    @foreach($upsells as $upsell)
                                    <tr>
                                        <td>{{ $upsell->branch_name}}</td>
                                        <td>{{ $upsell->transaction_no }}</td>
                                        <td>{{ $upsell->description }}</td>
                                        <td>{{ $upsell->qty}}</td>         
                                        <td>{{ $upsell->local_time }}</td>                         
                                    </tr>
                                    @endforeach                                                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection