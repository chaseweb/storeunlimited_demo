@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <div class="title_left">
                <a href="{{ route('sales.index')}}" class="btn btn-default">Back</a>
              </div>

            </div>

            <div class="x_panel">
                 
                  <div class="x_content">
                    <br>
                    <div class="form-horizontal form-label-left">
                    {!! Form::open(array('route' => array('sales.items.report'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                     <input type="hidden" class="form-control" value="{{$date_to}}" name="date_to">
                     <input type="hidden" class="form-control"  value="{{$date_from}}" name="date_from">
                      <input type="hidden" class="form-control"  value="{{$id}}" name="branch_code">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->company->company }}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->branch }}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date From </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ date("m/d/Y",  strtotime($date_from))}}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date To </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ date("m/d/Y",  strtotime($date_to))}}" placeholder="Disabled Input">
                        </div>
                      </div>
                       <div class="row">
                       <div class="col-md-3 col-xs-12">
                       </div>
                                 <div class="col-md-3 col-xs-12">
                                    
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div> 
                                </div>

                                
                            </div>

                    </div>
                  </div>
                </div>


   {!! Form::close() !!}  
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Item Details Report</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Barcode</th>
                                        <th>Itemcode</th>
                                        <th>Description</th>
                                        <th>Cost</th>
                                        <th>SRP</th>
                                        <th>Qty</th>
                                        <th>Gross Amount</th>
                                        <th>Net Amount</th>
                                        <th>Discount</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @if(count($items) > 0)
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->itemcode }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td class="right">{{ number_format($item->cost,2) }}</td>
                                        <td class="right">{{ number_format($item->srp,2) }}</td>
                                        <td class="right">{{ $item->qty }}</td>
                                        <td class="right">{{ number_format($item->gross_amount,2) }}</td>
                                        <td class="right">{{ number_format($item->net_amount,2) }}</td>
                                        <td class="right">{{ number_format($item->discount,2) }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="13">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection