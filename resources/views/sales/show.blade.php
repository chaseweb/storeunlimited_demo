@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <div class="title_left">
                <a href="{{ route('sales.index')}}" class="btn btn-default">Back</a>
              </div>

            </div>

             <div class="x_panel">
                 
                  <div class="x_content">
                    <br>
                    <div class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->company->company }}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->branch }}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date From </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ date("m/d/Y",  strtotime($date_from))}}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date To </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ date("m/d/Y",  strtotime($date_to))}}" placeholder="Disabled Input">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

            <div class="clearfix"></div>

             <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th>Company Name</th>
                                            <th>Branch Name</th> -->
                                            <th>Terminal #</th>
                                            <th>Transaction #</th>
                                            <th>Sales Man</th>
                                            <th>User</th>
                                            <th>Member</th>
                                            <th>Gross Amount</th>
                                            <th>Net Amount</th>
                                            <th>Sub Total Discount</th>
                                            <th>Total Item Discount</th>
                                            <th>Return Exchange Amount</th>
                                            <th>Cash Amount</th>
                                            <th>Card Amount</th>
                                            <th>Gift Amount</th>
                                            <th>Charge Amount</th>
                                            <th>Check Amount</th>
                                            <th>Account Amount</th>
                                            <th>ATM Amount</th>
                                            <th>Deffered Amount</th>
                                            <th>Other Payment</th>
                                            <th>Date Time</th>
                                            <th>Posting Time</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(count($items) > 0)
                                        @foreach($items as $item)
                                        <tr>
                                            <!-- <td>{{ $item->company_name }}</td>
                                            <td>{{ $item->branch_name }}</td> -->
                                            <td>{{ $item->terminal_no }}</td>
                                            <td>{{ $item->transaction_no }}</td>
                                            <td>{{ $item->sales_man }}</td>
                                            <td>{{ $item->user }}</td>
                                            <td>{{ $item->member }}</td>
                                            <td>{{ number_format($item->gross_amount,2) }}</td>
                                            <td>{{ number_format($item->net_amount,2) }}</td>
                                            <td>{{ number_format($item->sub_total_discount,2) }}</td>
                                            <td>{{ number_format($item->total_item_discount,2) }}</td>
                                            <td>{{ number_format($item->retex_amount,2) }}</td>
                                            <td>{{ number_format($item->cash_amount,2) }}</td>
                                            <td>{{ number_format($item->card_amount,2) }}</td>
                                            <td>{{ number_format($item->gift_amount,2) }}</td>
                                            <td>{{ number_format($item->charge_amount,2) }}</td>
                                            <td>{{ number_format($item->check_amount,2) }}</td>
                                            <td>{{ number_format($item->account_amount,2) }}</td>
                                            <td>{{ number_format($item->atm_amount,2) }}</td>
                                            <td>{{ number_format($item->deffered_amount,2) }}</td>
                                            <td>{{ number_format($item->other_payment,2) }}</td>
                                            <td>{{ $item->local_time }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td><a href="{{ route('sales.transaction', ['id' => $item->id, 'from' => strtotime($date_from), 'to' => strtotime($date_to)])}}"> Details</a></td>
                                            
                                        </tr>
                                        @endforeach
                                        
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
    </div>
    <!-- /page content -->
@endsection