@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif
    @if(isset($sel_terminals))
    var terminals = <?php echo json_encode($sel_terminals); ?>;
    @else
    var terminals = [];
    @endif

              $(document).ready(function() {

           

            $('#datatable1').dataTable({

             "aaSorting":[[3,"desc"]]
            });
    
               
          });
@endpush

@push('scripts')
    <script src="{{ asset("js/salessummary.js") }}"></script>

@endpush

@section('main_container')



    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Item Ranking Report</h3>
                </div>
            </div>

            <div class="clearfix"></div>



            {!! Form::open(array('route' => array('itemrank.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        <select class="form-control" data-placeholder="SELECT A BRANCH..." id="branch" name="branch[]" multiple="multiple" ></select>
                                    </div>
                                    
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>
                                    
                                </div>

                                
                            </div>

                            

                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Global Status</label>
                                        <select name="globals[]" id="globs" class="form-control" multiple="multiple">
                                            @foreach($globals as $glob)
                                                <option value="{{$glob}}">{!! $glob !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Campaign ID</label>
                                        <select name="campids[]" id="camps" class="form-control" multiple="multiple">
                                            @foreach($campids as $campid)
                                                <option value="{{$campid}}">{!! $campid !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group"> 
                                        <label for="branch">Metal</label>
                                        <select name="mets[]" id="metals" class="form-control" multiple="multiple">
                                            @foreach($metals as $metal)
                                                <option value="{{$metal}}">{!! $metal !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                     <div class="btn-group">
                                        <button type="submit" class="btn btn-success" value="1" name="submit">Process</button>
                                    </div>  
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}
                

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content table-responsive">
                                <table id="datatable1" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Barcode</th>
                                            <th>Item Code</th>
                                            <th>Description</th>
                                            <th>Global Status</th>
                                            <th>Campaign ID</th>
                                            <th>Metal</th>
                                            <th class="right">Qty</th>
                                            <th class="right">Total Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($items) > 0)
                                        @foreach($items as $item)
                                        <tr>
                                            <td>{{ $item->barcode }}</td>
                                            <td>{{ $item->itemcode }}</td>
                                            <td>{{ $item->description }}</td>
                                            <th>{{ $item->global_status }}</th>
                                            <th>{{ $item->campaign_id }}</th>
                                            <th>{{ $item->metal }}</th>
                                            <td class="right">{{ $item->qty}}</td>
                                            <td class="right">{{ number_format($item->net_amount,2) }}</td>
                                            
                                        </tr>
                                        @endforeach
                                        
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            
          </div>
    </div>
    <!-- /page content -->

@endsection
@push('scripts')
<script type="text/javascript">
    $('select#globs').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        
    });

    $('select#camps').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        
    });

    $('select#metals').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        
    });
</script>
@endpush